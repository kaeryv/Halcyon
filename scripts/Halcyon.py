import numpy as np
import codecs, json
import os
import uuid
import sys

def linearinterp( n_raw, k_raw, o_raw, o):
    #import matplotlib.pyplot as plt
    n_raw = np.array(n_raw)
    k_raw = np.array(k_raw)
    o_raw = np.array(o_raw)
    o = np.array(o)

    if not np.all(np.diff(o_raw) > 0):
        n_raw = np.flipud(n_raw)
        k_raw = np.flipud(k_raw)
        o_raw = np.flipud(o_raw)

    if(max(o) > max(o_raw) or min(o) < min(o_raw)):
        print("[Python] Preprocess error: Interpolation, no extrapolation.")
        return

    n = np.interp(o, o_raw, n_raw)
    k = np.interp(o, o_raw, k_raw)
    #plt.plot(o_raw, n_raw, 'r.')
    #plt.plot(o, n, 'b.')
    #plt.show()
    return (n,k)

def splineinterp( n_raw, k_raw, o_raw, o):
    from scipy.interpolate import interp1d
    #import matplotlib.pyplot as plt
    n_raw = np.array(n_raw)
    k_raw = np.array(k_raw)
    o_raw = np.array(o_raw)
    o = np.array(o)

    fn = interp1d(o_raw, n_raw, kind='linear')
    fk = interp1d(o_raw, k_raw, kind='linear')
    
    #plt.plot(o_raw, n_raw, 'r.')
    #plt.plot(o, fn(o), 'b.')
    #plt.show()

    return (fn(o), fk(o))
    


def randomizeFilename(name):
    return str(uuid.uuid4().fields[2]) + '_' + name

def getGeneratedFilesPath():
    return '../materials/generated/'

def getHalcyonHomePath():
    return '../'

def getMaterialsPath():
    return '../materials/'

def getMaterialsArchivePath():
    return '../materials/archive/'

def getJSONData(filepath):
    if not os.path.isfile(filepath):
        sys.exit()
    f = open(filepath, 'r')
    data = json.loads(f.read())
    f.close()
    return data
def generateJSON(nk, o, name):
    ran_name = randomizeFilename(os.path.basename(name))
    output_filename = getGeneratedFilesPath() + ran_name

    f = open(output_filename, 'w')
    if f.closed:
        return "FAILED"
    f.write(json.dumps({'n': nk[0], 'k': nk[1], 'o': o }, cls=NumpyEncoder, indent=4))
    f.close()
    return ran_name

def getJSONDataFromArchive(filename):
    return getJSONData(getMaterialsArchivePath() + filename)

def ev2nm(ev):
    return 1240/np.array(ev)*1e-9

def epsilon2nk(eps_r, eps_i):
    seps = np.sqrt(np.array(eps_r) + 1j * np.array(eps_i))
    n=np.real(seps)
    k=np.imag(seps)
    return ({'n':n,'k':k})
def check(nk_raw):
    import os
    os.chdir("../Halcyon/scripts/")
    print("runnning in", os.getcwd())
    relativepath = getMaterialsArchivePath() + nk_raw
    
    if not os.path.isfile(relativepath):
        print("File", relativepath, " does not exist.")
        return True
    
    return False

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



