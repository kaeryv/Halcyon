## @package KuboModel
#  Dielectric constant of graphene following the Kubo Model
#
#  More details.


## Main interface with c++ code
#
#  @param   omega Array containing angular frequencies values.
#  @return  a vector containing permittivity values associated to omega.
def KuboModel(omega):
    ret = [0.0, 0.0, 0.0]
    return ret