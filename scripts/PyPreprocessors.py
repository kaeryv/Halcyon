# -*- coding: utf-8 -*-


#linterp : Preprocessor for linear interpolation.
def linterp(nk_raw, wavelength):
    import os, sys
    reload(sys)
    import Halcyon as hl
    import numpy as np
    import codecs, json
    os.chdir("../Halcyon/scripts/")
    print("runnning in",os.getcwd())
    relativepath = hl.getMaterialsArchivePath() + nk_raw
    if not os.path.isfile(relativepath):
        print("File", relativepath, " does not exist.")
        return "FAILED"
    
    # getJSONData will load the file.json in a dic.
    raw = hl.getJSONData(relativepath)

    # We call the linear interpolation on n,k raw data along omega
    interpolated = hl.splineinterp( raw["n"], raw["k"], raw['o'], o)
    
    if interpolated == None:
        return "FAILED"

    ran_name = hl.randomizeFilename(os.path.basename(nk_raw))
    output_filename = hl.getGeneratedFilesPath() + ran_name

    f = open(output_filename, 'w')
    if f.closed:
        return "FAILED"
    f.write(json.dumps({'n': interpolated[0], 'k': interpolated[1], 'o': o }, cls=hl.NumpyEncoder, indent=4))
    f.close()
    
    return ran_name

def AnataseTiO2_Park2005(nk_raw, o):
    import os, sys
    reload(sys)
    import Halcyon as hl

    import numpy as np
    import codecs, json
    
    if hl.check(nk_raw):

        return "FAILED"
  
    raw = hl.getJSONDataFromArchive(nk_raw)
   
    # The raw values are epsilon real and imag


    wl = hl.ev2nm(raw['ev'])
    nk = hl.epsilon2nk(raw['eps_r'], raw['eps_i'])
    

    # We call the linear interpolation on n,k raw data along wavelength
    interpolated = hl.linearinterp( nk['n'], nk['k'], wl, o)
    
    if interpolated == None:
        return "FAILED"

    return hl.generateJSON(interpolated, o, nk_raw)
