#include <ConstantMaterial.hpp>

namespace Halcyon
{
    ConstantMaterial::ConstantMaterial() : 
                      Material()
    {}

    ConstantMaterial::ConstantMaterial(complex epsilon) : 
                      Material(),
                     _constant_epsilon(epsilon)
    {}

    ConstantMaterial::~ConstantMaterial()
    {}

    complex ConstantMaterial::epsilon(_dex_t index) const
    {
        return _constant_epsilon;
    }

    void ConstantMaterial::init(std::vector<real>& wavelength)
    {}
}