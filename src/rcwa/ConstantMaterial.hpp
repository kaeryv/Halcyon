/**
 * @file
 * @brief   Describes the ConstantMaterial Class
 * @details Description
*/
#pragma once

#include "Material.hpp"


namespace Halcyon
{
	/**
     * @brief  Non-dispersive Material.
     * @author Nicolas Roy
     * @details The dielectric constant is a complex constant.
     */
	class ConstantMaterial : public Material
	{
		public:
		/**
		 * @brief Main constructor.
		 */
			ConstantMaterial();

		/**
		 * @brief Main constructor from raw epsilon.
		 * @param epsilon Complex dielectric constant.
		 */
            ConstantMaterial(complex epsilon);

		/**
         * @brief Main destructor.
         */
			virtual ~ConstantMaterial();
		/**
         * @brief Copy of complex dielectric constant.
         * @param o The current wavelength indentifier.
         */
			complex epsilon(_dex_t index) const override;

		/**
         * @brief Setting-up the material.
         * @param wavelengths The wavelengths for which we want this material to provide epsilon.
		 * @details For this ConstantMaterial, the process is trivial.
         */
			void init(std::vector<real>& wavelength) override;

        private:
            complex _constant_epsilon;	///< Constant complex dielectric constant.
	};
}