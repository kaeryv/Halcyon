#include "Disk.hpp"


namespace Halcyon
{
    Disk::Disk() :
        Island(),
        x(0.0),
        y(0.0),
        radius(0.0)

    {}

    Disk::Disk(real x, real y, real radius, std::shared_ptr<Material> mat):
          Island(mat),
          x(x),
          y(y),
          radius(radius)
    {}

    Disk::~Disk()
    {}

    void Disk::compute_transform(const GGrid& reciprocal_lattice) 
    {
        _fTransform.init(reciprocal_lattice.exgrid().numRows(), reciprocal_lattice.exgrid().numCols());
        _fTransform.zeros();
        for (_dex_t i = 0; i < reciprocal_lattice.exgrid().numRows(); i++)
            for (_dex_t j = 0; j < reciprocal_lattice.exgrid().numCols(); j++) 
            {
                Vec2<real> v = reciprocal_lattice.exgrid()(i,j);
                real area = reciprocal_lattice.getUnitCellArea();

                if (std::abs(v.x) < 1e-20 && std::abs(v.y) < 1e-20) 
                { 
                    // Then when compute G(0,0) with appropriate, more stable formula
                    _fTransform(i,j) = (Math::twopi * (real)pow(radius, 2.0) / area * 0.5);
                }
                else 
                {	
                    // For other G(x,y), using classic fft
                    real norm = std::pow(std::pow(v.x, 2.0) + std::pow(v.y, 2.0), 0.5);

                    _fTransform(i,j) = ((Math::twopi * std::pow(radius, 2))
                        / (area * exp(Math::i*(x * v.x + y * v.y))) * ((real)j1(norm * radius) / (norm * radius)));

                    // Flushing badly scaled dissipative results to zero
                    if (std::abs(_fTransform(i,j).imag()) < 1e-20) 
                    {
                        _fTransform(i,j).imag(0.0);
                    }
                }
            }	
    }
}



