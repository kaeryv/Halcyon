/**
 * @file
 * @brief    Describes the Disk Class
 * @details
*/

#pragma once

#include "Island.hpp"



namespace Halcyon
{
    /**
     * @brief Cylindrical shaped island, responsible for Disk Fourier transform.
     * @author Nicolas Roy
     * @see Island
     */
    class Disk : public Island
    {
        public:

        /**
         * @brief Default constructor.
         */
            Disk();

        /**
         * @brief Copy constructor
         * @param disk The disk we are copying from.
         */
            Disk(const Disk& disk);

        /**
         * @brief Default destructor.
         */
            virtual ~Disk();

        /**
         * @brief Main constructor, from geometric parameters and material.
         * @param x			Center position along x axis in the layer's reference frame in meters.
         * @param y			Center position along y axis in the layer's reference frame in meters.
         * @param z			Center position along z axis in the layer's reference frame in meters.
         * @param radius	Radius of the disk in meters.
         * @param mat	    Material used for the disk
         *
         */
            Disk(real x, real y, real radius, std::shared_ptr<Material> mat);

        /**
         * @brief 					  Overridend method for computing Fourier transform over the GGrid
         * @param reciprocal_lattice  2D Grid containing all @vec{g}s  of the reciprocal lattice.
         * @todo                      Changing GGrid class name for better understanding.
         */
            void compute_transform(const GGrid& reciprocal_lattice) override;

        public:

            real x;			///< Disk center position.x [meters]
            real y;			///< Disk center position.y [meters]
            real radius;	///< Disk radius [meters]
    };
}

