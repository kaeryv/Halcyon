#include "DispersiveMaterial.hpp"

namespace Halcyon
{
    DispersiveMaterial::DispersiveMaterial() : Material()
    {

    }
    DispersiveMaterial::DispersiveMaterial(std::string material_name)
    {
        name = material_name;
    }    
   
    complex DispersiveMaterial::epsilon(_dex_t index) const
    {
        return _epsilon_values[index];
    }
    
            DispersiveMaterial::~DispersiveMaterial()
    {}

    void DispersiveMaterial::init(std::vector<real>& wavelength)
    {
        _epsilon_values.assign(wavelength.size(), 0.0);

        if(Material::IsPresentMaterial(name))
        {
            const json::json& mat = Material::FetchMaterial(name);

            if(json::IsKeyStringValue(mat, "type", "dispersive"))
            {
                // We really need to load a vector of eps values
                // But from where and how ?
                if(json::IsKeyStringValue(mat, "source", "archive"))
                {
                    // We load the values from a json array, but which ?
                    if(json::IsKeyStringPresent(mat, "file"))
                    {
                        std::string op;
                        if(json::IsKeyStringPresent(mat, "preprocess"))
                        {
                            // We need to perform additional operations on the raw data before use.
                            performMaterialToolchain(json::KeyAsString(mat, "file"), 
                                                     json::KeyAsString(mat, "preprocess"), 
                                                     wavelength);
                        }
                        else
                        {
                            // No operations ( never happens nuh ...)
                            performMaterialToolchain(json::KeyAsString(mat, "file"), 
                                                     "none", 
                                                     wavelength);
                        }
                    }
                    else Log::Fatal("With raw material source, you need to specify [source_file] as the raw array JSON filename.");
                }
                else if(json::IsKeyStringValue(mat, "source", "model"))
                {
                    ///@todo implement
                }

            }
            else if(mat["type"].is_string() && !mat["type"].get<std::string>().compare("dielectric"))
            {
                if(mat["n"].is_number() && mat["k"].is_number())
                {
                    const real& n = mat["n"];
                    const real& k = mat["k"];
                    complex eps = std::pow( n + Math::i * k, 2 );

                    _epsilon_values.assign(wavelength.size(), eps);

                    return;
                }
                
            }
            else
            {
                Log::Info("dez");
                if(mat["type"].is_string())
                    Log::Fatal("Material type " + mat["type"].get<std::string>() + " not recognized.");
                else
                    Log::Fatal("Material type not specified.");
            }
        }
        else
        {
            Log::Fatal("Material " + name + " not recognized by database, check spelling or add it.");
        }
    }
    void DispersiveMaterial::numValues()
    {
      
    }
    void DispersiveMaterial::loadEpsilonArrayFromJSON(std::string path)
    {
        json::json buffer;
        {
            std::ifstream input_filestream(path,std::ios::in);

            if(!input_filestream.is_open())
                Log::Fatal("Json : Raw NK file not found[" + path + "]");
            
            input_filestream >> buffer;
            input_filestream.close();
        }

        if(buffer.is_object())
        {
            if(buffer["o"].is_array() && buffer["n"].is_array() && buffer["k"].is_array())
            {
                if(buffer["o"].size() == buffer["n"].size() && buffer["o"].size() == buffer["k"].size())
                {
                    if(buffer["o"].size() == _epsilon_values.size())
                        for(_dex_t l =0 ; l < _epsilon_values.size(); ++l)
                        {
                            if(buffer["n"][l].is_number() && buffer["k"][l].is_number())
                            {
                                const real& k = buffer["k"][l];
                                const real& n = buffer["n"][l];
                                _epsilon_values[l] = pow( n + Math::i * k ,2 );
                            }  
                            else Log::Fatal("Wrong expression inside NK vector.");
                        }
                    else Log::Fatal("Not enough NK values, consider using interpolation option.");
                }
                else Log::Fatal("Inconsistent NK vectors length.");
            }
            else Log::Fatal("Missing member is JSON NK file.");
        }
    }

    void DispersiveMaterial::performMaterialToolchain(std::string path, std::string op, std::vector<real>& wavelength)
    {
        if(!op.compare("none"))
                loadEpsilonArrayFromJSON("../Halcyon/materials/archive/" + path);
        else
        {
            PythonMonolith::Singleton()->bindString(path);
            PythonMonolith::Singleton()->bindDoubleVector(wavelength);
            std::string py_return_value = PythonMonolith::Singleton()->run("PyPreprocessors", op);

            loadEpsilonArrayFromJSON("../materials/generated/" + py_return_value);
        }
            
            
    }
}