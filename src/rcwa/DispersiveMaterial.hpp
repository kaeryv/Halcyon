/**
 * @file
 * @brief   Describes the DispersiveMaterial Class
 * @details Contains Python Hassle
*/
#pragma once

#include "Material.hpp"
#include "Log.hpp"

#include "PythonMonolith.hpp"
#include "Math.hpp"
#include "json.hpp"

namespace Halcyon
{
    /**
     * @brief  Dispersive Material management.
     * @author Nicolas Roy
     * @details This class handles dielectric constant vector fetching from a material name and spectral qualifiers.
     * @todo The class is under construction.
     */
    class DispersiveMaterial : public Material
    {
        public:
        /**
         * @brief Default constructor.
         */
            DispersiveMaterial();

        /**
         * @brief Main constructor.
         * @param material_name Name of the material, it must lie in the database.
         */
            DispersiveMaterial(std::string material_name);
        
        /**
         * @brief Main destructor.
         */
            ~DispersiveMaterial();
   
        /**
         * @brief Copy of complex dielectric constant.
         * @param o The current wavelength indentifier.
         */
            complex epsilon(_dex_t index) const override;
            
        /**
         * @brief Setting-up the dispersive material.
         * @param wavelengths The wavelengths for which we want this material to provide epsilon.
         */
            void init(std::vector<real>& wavelength) override;

        /**
         * @brief Number of values the Material is currently considering.
         * @todo Not implemented.
         */
            void numValues();

        /**
         * @brief Loads JSON standard nk arrays from file.
         * @param path Absolute file path
         * @detail Safely loads JSON array from specified file.
         * @todo explicit the notion of standard Halcyon JSON nk array.
         */
            void loadEpsilonArrayFromJSON(std::string path);

        /**
         * @brief Performs operations on material source file.
         * @detail 
         * @todo Work in progress : Implement array of operations
         * @todo Better filesystem management and clear toolchain 
         */
            void performMaterialToolchain(std::string path, std::string op, std::vector<real>& wavelength);

            
        private:
            std::vector<complex> _epsilon_values;   ///< Buffer of dielectric constant spectrum.
            std::string name;                       ///< indentifier of the material.

            std::string generated_filename;         ///< Temp file the toolchain is working on @todo notyetused

    };
}