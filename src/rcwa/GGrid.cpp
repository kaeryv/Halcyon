#include "GGrid.hpp"

namespace Halcyon
{
	GGrid::GGrid(std::shared_ptr<LatticeParameters> params)
	{
		// Direct Network Base
		std::pair<vec2r, vec2r>& directBase = params->lattice;
		
		// Number of plane waves
		vec2t& truncation = params->latticeTruncation;
		
		vec2t dim = truncation;
		
		this->_data.init(dim.x, dim.y);
		
		// If both dimensions are even.
		if (dim.x % 2 == 0.0 && dim.y % 2 == 0.0) 
		{
			this->_bounds.x = (_dex_t)std::round((double)dim.x / 2.0);
			this->_bounds.y = (_dex_t)std::round((double)dim.y / 2.0);
			this->_ext_bounds = dim;
			this->_ext_data.init(2*dim.x, 2*dim.y);
		}
		// If both dimensions are odd.
		else if (dim.x % 2 == 1 && dim.y % 2 == 1)
		{
			this->_bounds.x = (_dex_t)std::round(((double)(dim.x - 1)) / 2.0);
			this->_bounds.y = (_dex_t)std::round(((double)(dim.y - 1)) / 2.0);
			this->_ext_bounds = dim+(-1);
			this->_ext_data.init(2*dim.x+1, 2*dim.y+1);
		}
		else
		{
			Log::Fatal("Both dimensions must have same parity.");
		}
		

		std::pair<vec2r, vec2r> conjugBase = getReciprocal(directBase);
		this->unitCellArea = std::abs(directBase.first.x*directBase.second.y - directBase.first.y*directBase.second.x);

		for (_dex_t i = 0; i < dim.x; ++i) 
		{	// rows :: x-axis
			int m1 = i - _bounds.x;
			for (_dex_t j = 0; j < dim.y; ++j) 
			{ //cols :: y-axis
				int m2 = j - _bounds.y;
				_data(i,j)= { 
					m1*conjugBase.first.x + m2*conjugBase.second.x, 
					m1*conjugBase.first.y + m2*conjugBase.second.y 
				};
			}
		}

		const vec2t& ext_dim  = _ext_data.size();
		for (_dex_t i = 0; i<ext_dim.x; ++i) 
		{
			int m1 = i - _ext_bounds.x;
			for (_dex_t j = 0; j<ext_dim.y; ++j) 
			{
				int m2 = j - _ext_bounds.y;
				_ext_data(i,j) =
				{
					m1*conjugBase.first.x + m2*conjugBase.second.x,
					m1*conjugBase.first.y + m2*conjugBase.second.y
				};
			}
		}
	}

	vec2i GGrid::coord(const _dex_t g) const
	{
		vec2i ret;
		vec2t dim = _data.size();
		int x = (int)floor((double)g / (double)dim.x	);
		ret = { -(int)_bounds.x + (int)g - x*(int)dim.x, -(int)_bounds.y + x };
		return ret;
	}


	std::pair<vec2r, vec2r> getReciprocal(std::pair<vec2r, vec2r> input)
	{
		std::pair<vec2r, vec2r> output;

		real det = input.first.x*input.second.y - input.first.y*input.second.x;

		det = Math::twopi / det;
		
		output.first =  {  input.second.y * det, -input.second.x * det };
		output.second = { -input.first.y  * det,  input.first.x  * det };

		return output;
	}
}