#pragma once


#include "Math.hpp"
#include "VectorUtils.hpp"
#include "Log.hpp"

#include "Matrix.hpp"


#include <vector>
#include <utility>
#include <complex>
#include <memory>



namespace Halcyon
{
	std::pair<vec2r, vec2r> getReciprocal(std::pair<vec2r, vec2r> input);

	/**
	 * @brief Set of parameters representing the numerical reciprocal lattice.
	 */
	struct LatticeParameters
	{
		std::pair<vec2r, vec2r> lattice;
		Vec2<_dex_t> latticeTruncation;
	};

	/**
	 * @brief Class representing the numerical reciprocal lattice.
	 */
	class GGrid {
		public:
			GGrid(){}
			GGrid(std::shared_ptr<LatticeParameters> params);
			~GGrid() {}

			vec2i coord(const _dex_t g) const;
			const vec2t& getExternalBoundaries() const { return _ext_bounds; }
			const vec2t& getBoundaries()         const { return _bounds; }

			Matrix<vec2r>& grid() { return this->_data; }
			const Matrix<vec2r>& grid() const { return this->_data; }

			Matrix<vec2r>& exgrid() { return this->_ext_data; }
			const Matrix<vec2r>& exgrid() const { return this->_ext_data; }

			const real getUnitCellArea()   const { return unitCellArea; }

		public:
			//std::pair<vec2r, vec2r> directBase;
			//std::pair<vec2r, vec2r> conjugBase;
			real unitCellArea;

		protected:
			vec2t _dim, _bounds, _ext_dim, _ext_bounds;
			_dex_t _length, _ext_length;

			Matrix<vec2r> _data;
			Matrix<vec2r> _ext_data;
	};
}