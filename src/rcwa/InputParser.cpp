#include "InputParser.hpp"


namespace Halcyon
{
    InputParser::InputParser()
    {}

    InputParser::~InputParser()
    {}

    SequencialInputParser::SequencialInputParser(std::string incident_file, std::string epsilon_file) :
        InputParser()
    {
        storage = std::make_shared<InputStorageContainer>();
        loadIncidentFile(incident_file);
        loadEpsilonFile(epsilon_file);
    }

    SequencialInputParser::~SequencialInputParser()
    {

    }

    bool SequencialInputParser::loadIncidentFile(std::string path)
    {
        real r_temp;
        std::string temp;

        Log::Info("Reading incident wave data : [" + path + "].");

        

        std::ifstream file(path, std::ios::in);

        if (!file.is_open())
        {
            Log::Fatal("Could not open incident file.");
        }

        file.precision(7);      // 6 characters mantissa
        file >> std::scientific;
        file >> r_temp;
        storage->nLambda = floor(r_temp);
        getline(file, temp);
        

        file >> storage->lambdaMin >> storage->lambdaMax;
        getline(file, temp);


        file >> storage->nTheta;      // Azimuth
        getline(file, temp);
        file >> storage->thetaMin >> storage->thetaMax;
        getline(file, temp);
        file >> storage->nPhi;        // Horiz
        getline(file, temp);
        file >> storage->phiMin >> storage->phiMax;
        getline(file, temp);


        getline(file, temp); //power line
        getline(file, temp); //radius line

        storage->polarization = std::make_shared<JonesVector>();
        file >> r_temp;
        storage->polarization->first.real(r_temp); // multilayer polarisation
        file >> r_temp;
        storage->polarization->first.imag(r_temp);
        getline(file, temp);
        file >> r_temp;
        storage->polarization->second.real(r_temp); // param polarisation
        file >> r_temp;
        storage->polarization->second.imag(r_temp);
        
        file.close();

        return true;
    }

    bool SequencialInputParser::loadEpsilonFile(std::string path)
    {
        std::string buffer, identifier;
        real r_buffer, r_temp;
        std::ifstream file;
        bool stop = false;
        _dex_t n_constant_material;

        Log::Info("Reading epsilon function data : [" + path + "].");
        file.open(path, std::ios::in);

        if (!file.is_open())
        {
            Log::Error("Unable to open input file.");
        }

        file.precision(6);
        file >> std::scientific;

        std::string line;
        {
            std::getline(file, line);
            std::istringstream iss(line);
            complex buffer;

            storage->outerMediums = std::make_shared<OuterMediums>();

            if ((iss >> buffer))
            {
                storage->outerMediums->first = std::make_shared<ConstantMaterial>(buffer);
            }
            else
            {
                Log::Info("Failed to read epsilon incident.");
            }
        }

        {
            std::getline(file, line);
            std::istringstream iss(line);
            complex c_buffer;
            std::string s_buffer;
            if ((iss >> s_buffer >> c_buffer))
            {
                storage->outerMediums->second = std::make_shared<ConstantMaterial>(c_buffer);
            }
            else
            {
                Log::Error("Failed to read epsilon emergent.");
            }
        }

        {
            std::getline(file, line);
            std::istringstream iss(line);
            Vec2<real> vector;

            storage->params = std::make_shared<LatticeParameters>();

            if ((iss >> vector))
            {
                storage->params->lattice.first = vector;
            }
            else
            {
                Log::Error("Failed to read a1.");
            }
        }

        {
            std::getline(file, line);
            std::istringstream iss(line);
            Vec2<real> vector;

            if ((iss >> vector))
            {
                storage->params->lattice.second = vector;
            }
            else
            {
                Log::Error("Failed to read a2.");
            }
        }

        {
            std::getline(file, line);
            std::istringstream iss(line);
            Vec2<_dex_t> vector;

            if ((iss >> vector))
            {
                storage->params->latticeTruncation = vector;
            }
            else
            {
                Log::Error("Failed to read truncation.");
            }
        }

        storage->multilayer = std::make_shared<Multilayer>();
        std::shared_ptr<Layer> current_layer = nullptr;
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string identifier;

            iss >> identifier;
            //std::cout << identifier << std::endl;
            if (!identifier.compare("set"))
            {
                real depth, sampling;
                complex epsilon = (0.0,0.0);
                std::string  name, eps_name;
                iss >> name >> depth >> sampling >> eps_name >> epsilon;
                //std::cout << " " << name <<" " << depth << " "<< eps_name << epsilon <<std::endl;

                // We end-up pushing the layer with the Material Abstract Reference
                
                current_layer = std::make_shared<Layer>(depth, MakeMaterial(eps_name, epsilon),sampling);
                storage->multilayer->push_back(current_layer);
            }
            else if (!identifier.compare("uniform") && current_layer != nullptr)
            {
                std::shared_ptr<Island> island = std::make_shared<Uniform>(current_layer->hostMaterial());
            }
            else if (!identifier.compare("disk")  && current_layer != nullptr)
            {
                std::string  name, eps_name;
                real x, y, radius;

                complex epsilon;
                iss >> x >> y >> radius >> eps_name >> epsilon;
                //std::cout << x << y << radius << eps_name << epsilon << std::endl;
                std::shared_ptr<Island> island = std::make_shared<Disk>(x, y, radius, MakeMaterial(eps_name, epsilon));
                current_layer->addIsland(island);
            }
            else if(!identifier.substr(0,1).compare("%"))
            {
                /* Comment line */
            }
            else
            {
                Log::Warning("Indentifier not recognized: '" + identifier + "' line not parsed.");
            }
        }
    }


    std::shared_ptr<Material> InputParser::MakeMaterial(std::string name, complex epsilon)
    {
        std::string material_name;
        if(!name.compare("constant"))
        {
            // Constant material name be like constant(1.5,2.3)
            material_name = name + std::to_string(epsilon.real()) + std::to_string(epsilon.imag());

            //We need to know if it is already registered
            std::map<std::string, std::shared_ptr<Material>>::iterator it = storage->materials.find(material_name);
            if(it == storage->materials.end())
            {
                storage->materials[material_name] = std::make_shared<ConstantMaterial>(epsilon);
            }
        }
        else
        {
            // Constant material name be like Ti02_Anatase, user defined.
            material_name = name;

            //We need to know if it is already registered
            std::map<std::string, std::shared_ptr<Material>>::iterator it = storage->materials.find(material_name);
            if(it == storage->materials.end())
            {
                storage->materials[material_name] = std::make_shared<DispersiveMaterial>(material_name);
            }
        }
        Log::Info("Calling for material: "  + material_name);
        return storage->materials[material_name];
    }
}