/**
 * @file
 * @brief   Description of the RCWAInputStorage Class
 * @details
*/

#pragma once


#include "Log.hpp"

#include "InputStorageContainer.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <memory>
#include <map>

namespace Halcyon
{
    /**
     * @brief Abstract InputStorage Class
     * @author Nicolas Roy
     * @details Override this class to create your own input files parser.
     * If you want to add more inputs to the InputStorageContainer, this is not permitted.
     * You can however call local python Monolith or JSON data from an overriden superclass.
     * Do not fear a blue dragon eating you in your sleep if doing so.
     * Just post feature request. Or push ... Pay me one coffee.
     */
    class InputParser
    {
        friend class HalcyonProject;

        public:
        /**
         * @brief Default contructor
         */
            InputParser();
        /**
         * @brief Default Destructor
         */
            virtual	~InputParser();
        protected:

            std::shared_ptr<InputStorageContainer> storage;                             ///< Storage for all input of an HalcyonProject
            std::shared_ptr<Material> MakeMaterial(std::string name, complex epsilon);  ///< Manage the above database, these two need rework
    };

    /**
     * @brief Sequential input files parser
     * @author Nicolas Roy
     * @details This parser was designed to read Olivier Deparis's TMAT original file format.
     * These input files present like two <name>.txt files, one for incident, incident.txt ans eps.txt for structure data typically.
     * This format is supported in the long term.
     */
    class SequencialInputParser : public InputParser
    {
        public:
        /**
         * @brief Master Constructor
         * @param incident_file incident.txt - like filepath
         * @param epsilon_file eps.txt - like filepath
         */
            SequencialInputParser(std::string incident_file,
                                   std::string epsilon_file);

        /**
         * @brief Default Destructor
         */
            virtual	~SequencialInputParser();

        protected:
        /**
         * @brief Load the incident file
         * @param path file path
         */
            bool loadIncidentFile(std::string path);

        /**
         * @brief Load the structure file
         * @param path file path
         */
            bool loadEpsilonFile(std::string path);
    };

    /**
     * @brief MetaData JSON input file parser.
     * @author Nicolas Roy
     * @todo Implement
     * @details This input file parses JSON input file, these are compatible with more TP software/libs.
     */
    class MetaInputParser : public InputParser
    {
        public:
            MetaInputParser(std::string incident_file);

            bool loadMetaFile(std::string path);

            virtual ~MetaInputParser();
    };
}