#pragma once

#include "mkl_config.h"
#include "Disk.hpp"

#include "Uniform.hpp"
#include "Layer.hpp"
#include "DispersiveMaterial.hpp"
#include "ConstantMaterial.hpp"

namespace Halcyon
{
    typedef std::pair<complex, complex> JonesVector;
    typedef std::pair<std::shared_ptr<ConstantMaterial>, std::shared_ptr<Material>> OuterMediums;
    typedef std::vector<std::shared_ptr<Layer>> Multilayer;

    /**
     * @brief This structure contains all the inputs required by Halcyon.
     * @author Nicolas Roy
     * @details This structure summarizes every input, it needs to be contained in a class able to fill it.
     * This object is designed to be destroyed one it served the creation of operationnal types.
     * It contains shared pointers for data that is already naturally operational type.
     * The other variables which are not required after initialization will be deallocated.
     */

    struct InputStorageContainer
    {
        /// Spectral data
        real lambdaMin;                                                    ///< Valeur de départ pour la longeur d'onde.
        real lambdaMax;                                                    ///< Valeur finale pour la longeur d'onde.
        _dex_t nLambda;                                                    ///< Nombre de valeurs de lambda à générer.

        real thetaMin;                                                     ///< Angle azimutal.[deg]
        real thetaMax;
        _dex_t nTheta = 1;

        real phiMin;                                                       ///< Angle horizontal.[deg]
        real phiMax;
        _dex_t nPhi = 1;

        /// Polarization
        std::shared_ptr<std::pair<complex, complex>> polarization;         ///< Polarization of the incident wave.
        real E0 = 1.0;                                                     ///< Incident field amplitude [deprecated]

        /// Materials
        std::shared_ptr<std::pair
            <std::shared_ptr<ConstantMaterial>,
             std::shared_ptr<Material>>> outerMediums;                     ///< Pair of dielectric constants, incident and emergent medium.
        std::map<std::string, std::shared_ptr<Material>> materials;        ///< Map of materials to load.

        /// Structure
        std::shared_ptr<LatticeParameters> params;
        std::shared_ptr<std::vector<std::shared_ptr<Layer>>>  multilayer;  ///< Array of Layer s
    };
}