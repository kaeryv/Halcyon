/**
 * @file
 * @brief Describes Island
 */

#pragma once

#include "GGrid.hpp"
#include "VectorUtils.hpp"
#include "Material.hpp"
#include "Matrix.hpp"

#include <memory>
#include <vector>

namespace Halcyon
{
    /**
     * @brief  Abstract Island.
     * @author Nicolas Roy
     * @details This class is the ABC for all Islands.
     */
    class Island
    {
        public:
        /**
         * @brief Main constructor.
         */
            Island(std::shared_ptr<Material> mat) : _material(mat)
            {}

        /**
         * @brief Default constructor.
         */
            Island()
            {}

        /**
         * @brief Default destructor.
         */
            virtual ~Island() 
            { 
                clear(); 
            }

        /**
         * @brief Virtual Deferred Computation of the Fourier transform
         * @param conj_network  Reciprocal Lattice
         */
            virtual void compute_transform(const GGrid& conj_network) = 0;
            
            
            void clear()
            {
          
            }

            
            const complex& operator()(const _dex_t i, const _dex_t j) const 
            { 
                return this->_fTransform(i,j); 
            }


            complex& operator()(const _dex_t i, const _dex_t j) 
            { 
                return this->_fTransform(i,j); 
            }
            
            complex epsilon(_dex_t o) 
            { 
                return _material->epsilon(o); 
            }

            std::shared_ptr<Material> material()
            {
                return _material;
            }

        protected:

            std::shared_ptr<Material> _material;    ///< Reference to material in DB
            Matrix<complex>    _fTransform;         ///< Buffer containing latest Fourier Transform
    };
}