#include "Layer.hpp"

namespace Halcyon
{
    Layer::Layer(real depth, 
                 complex epsilon, 
                 _dex_t slices) : 
                 _depth(depth),
                 _slices(slices)
    {
        _host_material = std::make_shared<ConstantMaterial>(epsilon);
    }

    Layer::Layer(real depth,
                 std::shared_ptr<Material> mat,
                 _dex_t slices) : 
                 _depth(depth),
                 _host_material(mat),
                 _slices(slices)
    {}

    Layer::~Layer()
    {}

    void Layer::addIsland(std::shared_ptr<Island> island)
    {
        _islands.push_back(island);
    }

    void Layer::bake_islands(std::shared_ptr<GGrid> conjNetwork)
    {
        for (std::shared_ptr<Island> island : _islands) {
            island->compute_transform(*conjNetwork);
        }

        // We tell buffers things are going sideway soon
        epsilon.init(conjNetwork->exgrid().numRows(), conjNetwork->exgrid().numCols());
        iepsilon.init(conjNetwork->exgrid().numRows(), conjNetwork->exgrid().numCols());
    }

    void Layer::computeScatteringMatrix(const GGrid & g_vectors, 
                                        const WaveVector & k, 
                                        const Matrix<complex>& U, 
                                        const Matrix<complex>& VI,
                                        _dex_t o)
    {
        Vec2<_dex_t> bounds = g_vectors.getExternalBoundaries();
        Vec2<_dex_t> dims = g_vectors.exgrid().size();
        
        // Clearing vectors (unsing assingment operators beneath)

        epsilon.zeros();
        iepsilon.zeros();


        
        // Computing epsilon and 1 over epsilon Fourier transforms

        for (_dex_t i = 0; i < g_vectors.exgrid().numRows(); i++)
            for (_dex_t j = 0; j < g_vectors.exgrid().numCols(); j++) 

            {
                for (std::shared_ptr<Island> island : _islands) 
                {
                    epsilon(i,j) += (island->epsilon(o) - hostMaterial()->epsilon(o)) * (*island)(i,j);
                }
            }

        epsilon(bounds.x, bounds.y) =  hostMaterial()->epsilon(o) + epsilon(bounds.x, bounds.y);

        complex iepsHost = (complex) 1.0 /  hostMaterial()->epsilon(o);
        for (_dex_t i = 0; i < g_vectors.exgrid().numRows(); i++)
            for (_dex_t j = 0; j < g_vectors.exgrid().numCols(); j++) 
            {
                for (std::shared_ptr<Island> island : _islands) {
                    iepsilon(i,j) += (((complex)1.0 / island->epsilon(o)) - iepsHost)*(*island)(i,j);
                }
            }
        iepsilon(bounds.x, bounds.y) = iepsHost + iepsilon(bounds.x, bounds.y);

        
        MatrixBuilder::generateA(g_vectors, k, epsilon, iepsilon, A);

        A.scale(-_depth / (real)_slices);

        this->S_matrix = MatrixBuilder::generateS(g_vectors, (U * A.exp())*VI);

        // Powering slices
        for (_dex_t k = 0; k < (_dex_t) std::log2(_slices); ++k) 
        {
            this->S_matrix = MatrixBuilder::multS(this->S_matrix, this->S_matrix);
        }
        
    }

    Matrix<complex>& Layer::scatteringMatrix()
    {
        return S_matrix;
    }

    std::shared_ptr<Material> Layer::hostMaterial()
    {
         return _host_material;
    }
}
