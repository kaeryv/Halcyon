/**
 * @file
 * @brief Describes Layer
 */

#pragma once

/// Local dependencies
#include "Matrix.hpp"
#include "Island.hpp"
#include "GGrid.hpp"
#include "WaveVector.hpp"
#include "MatrixBuilder.hpp"
#include "ConstantMaterial.hpp"

/// STL dependencies
#include <memory>
#include <vector>

namespace Halcyon
{
     /**
     * @brief  Structured optical material Layer.
     * @author Nicolas Roy 
     * @details This class is responsible for providing the S matrix out of physical arguments.
     */
    class Layer
    {
        public:
        /**
         * @brief Old constructor using direct epsilon complex value.
         * @param depth The depth of the layer.
         * @param epsilon The dielectric constant of the layer
         * @param slices The number of numerical slices
         * @deprecated
         */
            Layer(real depth, 
                  complex epsilon, 
                  _dex_t slices);

        /**
         * @brief Main constructor using Material shared pointer.
         * @param depth The depth of the layer.
         * @param mat The material reference in the DB
         * @param slices The number of numerical slices
         */
            Layer(real depth,
                  std::shared_ptr<Material> mat,
                  _dex_t slices);

        /**
         * @brief Default destructor.
         */
            virtual ~Layer();

        /**
         * @brief Append Island as child of the Layer.
         * @param island The island reference in DB.
         */
            void addIsland(std::shared_ptr<Island> island);
        
        /**
         * @brief Main constructor using Material shared pointer.
         * @param depth The depth of the layer.
         * @param mat The material reference in the DB
         * @param slices The number of numerical slices
         */
            void bake_islands(std::shared_ptr<GGrid> reciprocal_lattice);
        
        /**
         * @brief Deferred computation of the scattering matrix.
         * @param conjNetwork The depth of the layer.
         * @param k The current wave vector.
         * @param U The first base change matrix.
         * @param VI The second base change matrix.
         * @param o Frequecy domain index. 
         */    
            void computeScatteringMatrix(const GGrid& conjNetwork, 
                                         const WaveVector& k, 
                                         const Matrix<complex>& U, 
                                         const Matrix<complex>& VI,
                                         _dex_t o);
        /**
         * @brief Accessor to the scattering matrix.
         * @return S Reference to the scattering matrix.  
         */    
            Matrix<complex>& scatteringMatrix();

         /**
         * @brief Reference share for the material.
         * @return S Pointer to the scattering matrix.  
         */    
            std::shared_ptr<Material> hostMaterial();
            
            
        protected:
        
            Matrix<complex> T_matrix;                   ///< Transformed transfer matrix buffer.
            Matrix<complex> S_matrix;                   ///< Scattering matrix buffer.
            Matrix<complex> A;                          ///< Transfer matrix buffer.

            
        private:

            std::shared_ptr<Material> _host_material;   ///< Host material pointer.
            _dex_t _slices;                        ///< Numerical slices.
            
            real _depth;                                ///< Depth of the layer.
            Matrix<complex> epsilon;               ///< Dielectric buffer.
            Matrix<complex> iepsilon;              ///< Inverse dielectric buffer.
            std::vector<std::shared_ptr<Island>> _islands;              ///< Array of Islands.
    };
}