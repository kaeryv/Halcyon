#include "Material.hpp"

namespace Halcyon
{
    Material::Material()
    {}
    

    Material::~Material()
    {}

    void Material::LoadMaterialsDataBase(std::string path)
    {
        std::ifstream input_filestream(path);
        if(!input_filestream.is_open())
            Log::Fatal("JSON: File not found[" + path + "]");

        input_filestream >> Database;
        input_filestream.close();
    }

    bool Material::IsPresentMaterial(std::string material_name)
    {
        Log::Info("Material [" + material_name + "] was found in database.");
        return Material::Database["materials"][material_name].is_object();
    }

    const json::json& Material::FetchMaterial(std::string material_name)
    {
        return Material::Database["materials"][material_name];
    }

    json::json Material::Database = json::json();
}