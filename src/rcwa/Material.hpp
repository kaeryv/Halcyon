/**
 * @file
 * @brief   Describes the Material Class
 * @details Desciption
*/
#pragma once

#include "mkl_config.h"
#include "json.hpp"
#include "Log.hpp"

#include <vector>
#include <fstream>

namespace Halcyon
{
    /**
     * @brief  Abstract Material, stores static data common to all materials.
     * @author Nicolas Roy
     * @details This class is the ABC for all Materials, it contains the Monotlithic Database.
     * @todo Improve the Monolithic database with true Monolithic behaviour and no scotch-tape.
     */

    class Material
    {
       
        public:
        /**
         * @brief Default constructor.
         */
            Material();
        /**
         * @brief Main destructor.
         */
            virtual ~Material();

        /**
         * @brief Pure virtual interface for getting complex dielectric constant.
         * @param o The current wavelength indentifier.
         */
            virtual complex epsilon(_dex_t o) const = 0;

        /**
         * @brief Pure virtual interface for setting-up the material.
         * @param wavelengths The wavelengths for which we want this material to provide epsilon.
         */
            virtual void init(std::vector<real>& wavelengths) = 0;
            
        public:
        /**
         * @brief Static materials common databse loader.
         * @param path The path to database.json
         * @deprecated Will be replaced by Monolithic behaviour.
         */
            static void LoadMaterialsDataBase(std::string path);

        /**
         * @brief Looks for given material name in database.
         * @param material_name
         * @return boolean True it is found.
         */
            static bool IsPresentMaterial(std::string material_name);

        /**
         * @brief Fetches given material reference in database.
         * @param material_name
         * @return json Database entry reference for material.
         * @pre Must perform IsPresentMaterial before any Fetch.
         */
            static const json::json& FetchMaterial(std::string material_name);
        
        public:

            static json::json   Database;   ///< The static database
    };
}