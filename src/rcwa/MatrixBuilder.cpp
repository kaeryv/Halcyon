#include "MatrixBuilder.hpp"

namespace Halcyon
{
	Vec2<int> g2coord(_dex_t g, Vec2<_dex_t> bounds, Vec2<_dex_t> dim) {
		
		Vec2<int> ret;
		int x = (int)floor((double)g / (double)dim.y);
		ret.x = -bounds.x + x;
		ret.y = -bounds.y + g - x*dim.y;
		return ret;
	}

	void MatrixBuilder::generateU(const GGrid & g_vectors, const WaveVector & wave, const complex & epsilon, Matrix<complex>& matrix_)
	{
		_dex_t ng = g_vectors.grid().length();

		if(!matrix_.hasContent()) matrix_.init(4 * ng, 4 * ng);
		matrix_.zeros();
		real mu0C = Math::mu0*Math::c;
		complex hU = mu0C * pow(epsilon, -0.5);

		const ComplexBase& mu = wave.accessPolarizationBaseMuIncident();
		const ComplexBase& eta = wave.accessPolarizationBaseEta();

		const Matrix<complex>& kz = wave.accessWaveVectorIncidentZ();

	#   pragma omp parallel for
		for (int g = 0; g < ng; g++) {

			vec2i m = g_vectors.coord(g);
			_dex_t i = m.x +  g_vectors.getBoundaries().x;
			_dex_t j = m.y + g_vectors.getBoundaries().y;

			complex dg = ((sqrt(epsilon)*Math::pi) / wave.wavelength) / kz(i,j);
			matrix_(g, g) = dg*mu.second(i,j);
			matrix_(ng + g, g) = dg*eta.second(i,j);
			matrix_(2 * ng + g, g) = dg*mu.second(i,j);
			matrix_(3 * ng + g, g) = -dg*eta.second(i,j);
			matrix_(g, ng + g) = -dg*mu.first(i,j);
			matrix_(ng + g, ng + g) = -dg*eta.first(i,j);
			matrix_(2 * ng + g, ng + g) = -dg*mu.first(i,j);
			matrix_(3 * ng + g, ng + g) = dg*eta.first(i,j);
			matrix_(g, 2 * ng + g) = -hU*dg*eta.second(i,j);
			matrix_(ng + g, 2 * ng + g) = hU*dg*mu.second(i,j);
			matrix_(2 * ng + g, 2 * ng + g) = hU*dg*eta.second(i,j);
			matrix_(3 * ng + g, 2 * ng + g) = hU*dg*mu.second(i,j);
			matrix_(g, 3 * ng + g) = hU*dg*eta.first(i,j);
			matrix_(ng + g, 3 * ng + g) = -hU*dg*mu.first(i,j);
			matrix_(2 * ng + g, 3 * ng + g) = -hU*dg*eta.first(i,j);
			matrix_(3 * ng + g, 3 * ng + g) = -hU*dg*mu.first(i,j);

		}
	}

	void MatrixBuilder::generateVI(const GGrid & g_vectors, const WaveVector & wave, const complex & epsilon, Matrix<complex>& matrix_)
	{
		_dex_t ng = g_vectors.grid().length();
		if (!matrix_.hasContent()) matrix_.init(4 * ng, 4 * ng);
		real mu0C = Math::mu0*Math::c;
		complex hV = pow(epsilon, 0.5) / mu0C;

		const ComplexBase& eta = wave.accessPolarizationBaseEta();
		const ComplexBase& mu = wave.accessPolarizationBaseMuIncident();

		matrix_.zeros();

	#	pragma omp parallel for
		for (int g = 0; g<ng; g++) {
						vec2i m = g_vectors.coord(g);
			_dex_t i = m.x +  g_vectors.getBoundaries().x;
			_dex_t j = m.y + g_vectors.getBoundaries().y;
			

			matrix_(g, g) =  eta.first(i,j);

			matrix_(ng + g, g) = eta.second(i,j);

			matrix_(2 * ng + g, g) = hV*mu.first(i,j);
			matrix_(3 * ng + g, g) = hV*mu.second(i,j);

			matrix_(g, ng + g) = -mu.first(i,j);
			matrix_(ng + g, ng + g) = -mu.second(i,j);

			matrix_(2 * ng + g, ng + g) = hV*eta.first(i,j);
			matrix_(3 * ng + g, ng + g) = hV*eta.second(i,j);
			matrix_(g, 2 * ng + g) = eta.first(i,j);
			matrix_(ng + g, 2 * ng + g) = eta.second(i,j);
			matrix_(2 * ng + g, 2 * ng + g) = -hV*mu.first(i,j);
			matrix_(3 * ng + g, 2 * ng + g) = -hV*mu.second(i,j);
			matrix_(g, 3 * ng + g) =  mu.first(i,j);
			matrix_(ng + g, 3 * ng + g) =  mu.second(i,j);
			matrix_(2 * ng + g, 3 * ng + g) = hV*eta.first(i,j);
			matrix_(3 * ng + g, 3 * ng + g) = hV*eta.second(i,j);

		}
		
	}

	void MatrixBuilder::generateVE(const GGrid & g_vectors, const WaveVector & wave, const complex & epsilon, Matrix<complex>& matrix_)
	{
		_dex_t ng = g_vectors.grid().length();
		if (!matrix_.hasContent()) matrix_.init(4 * ng, 4 * ng);
		real mu0C = Math::mu0*Math::c;
		complex hV = pow(epsilon, 0.5) / mu0C;
		matrix_.zeros();
		
		const ComplexBase& eta = wave.accessPolarizationBaseEta();
		const ComplexBase& mu = wave.accessPolarizationBaseMuEmergent();

	#	pragma omp parallel for
		for (int g = 0; g<ng; g++) {
			vec2i m = g_vectors.coord(g);
			_dex_t i = m.x +  g_vectors.getBoundaries().x;
			_dex_t j = m.y + g_vectors.getBoundaries().y;

			matrix_(g, g) = eta.first(i,j);
			matrix_(ng + g, g) = eta.second(i,j);
			matrix_(2 * ng + g, g) = hV*mu.first(i,j);
			matrix_(3 * ng + g, g) = hV*mu.second(i,j);
			matrix_(g, ng + g) = -mu.first(i,j);
			matrix_(ng + g, ng + g) = -mu.second(i,j);
			matrix_(2 * ng + g, ng + g) = hV*eta.first(i,j);
			matrix_(3 * ng + g, ng + g) = hV*eta.second(i,j);
			matrix_(g, 2 * ng + g) = eta.first(i,j);
			matrix_(ng + g, 2 * ng + g) = eta.second(i,j);
			matrix_(2 * ng + g, 2 * ng + g) = -hV*mu.first(i,j);
			matrix_(3 * ng + g, 2 * ng + g) = -hV*mu.second(i,j);
			matrix_(g, 3 * ng + g) = mu.first(i,j);
			matrix_(ng + g, 3 * ng + g) =  mu.second(i,j);
			matrix_(2 * ng + g, 3 * ng + g) = hV*eta.first(i,j);
			matrix_(3 * ng + g, 3 * ng + g) = hV*eta.second(i,j);
		}
	}

	/** \author Nicolas Roy */ 
	Matrix<complex> MatrixBuilder::assemble(const Matrix<complex>& pp, const Matrix<complex>& pm, const Matrix<complex>& mp, const Matrix<complex>& mm)
	{
		// You came here to suffer ?
		Matrix<complex> result(pp.numCols() * 2, pp.numRows() * 2);

		// Fine, let's nest some loops BRUTEFORCE
		_dex_t tng = pp.numRows();
		for (_dex_t i = 0; i < pp.numRows(); i++) 
		{
			for (_dex_t j = 0; j < pp.numCols(); j++) 
			{
				result(i, j) =  pp(i, j);
			}
		}
		for (_dex_t i = 0; i < pm.numRows(); i++) 
		{
			for (_dex_t j = 0; j < pm.numCols(); j++) 
			{
				result(i, tng + j) =  pm(i, j);
			}
		}
		for (_dex_t i = 0; i < mp.numRows(); i++) 
		{
			for (_dex_t j = 0; j < mp.numCols(); j++) 
			{
				result(tng + i, j) =  mp(i, j);
			}
		}
		for (_dex_t i = 0; i < mm.numRows(); i++) 
		{
			for (_dex_t j = 0; j < mm.numCols(); j++) 
			{
				result(tng + i, tng + j) =  mm(i, j);
			}
		}

		return result;
	}

	/** \author Nicolas Roy */ 
	Matrix<complex> MatrixBuilder::generateS(const GGrid& g, const Matrix<complex>& T)
	{
		_dex_t ng = g.grid().length();

		Matrix<complex> T_pp = T.crop(0,0,2*ng,2*ng);
		Matrix<complex> T_pm = T.crop(0,2*ng,2*ng,2*ng);
		Matrix<complex> T_mp = T.crop(2 * ng, 0, 2 * ng, 2 * ng);
		Matrix<complex> T_mm = T.crop(2 * ng, 2 * ng, 2 * ng, 2 * ng);

		// S(plus, plus) = inv(T(plus, plus));
		Matrix<complex> S_pp = T_pp.inv();
		// S(plus, minus) = -S(plus, plus)*T(plus, minus);
		Matrix<complex> S_pm = (Matrix<complex>)(S_pp*T_pm)*((real)-1.0);
		//S(minus, plus) = T(minus, plus)*S(plus, plus);
		Matrix<complex> S_mp = T_mp*S_pp;
		//S(minus, minus) = T(minus, minus) + T(minus, plus)*S(plus, minus);
		Matrix<complex> S_mm = T_mm + T_mp*S_pm;
		
		Matrix<complex> S(4 * ng, 4 * ng);
		S = assemble(S_pp, S_pm, S_mp, S_mm);
		return S;
	}

	/** \author Nicolas Roy */ 
	Matrix<complex> MatrixBuilder::multS(Matrix<complex>& S1, Matrix<complex>& S2)
	{
		/**
		 * \todo Improve
		 * Note: This operation is memory unefficient as you can see
		 * Too much copying
		 * pp stands for ++, mm for -- submatrices, etc.
		 */

		// This is awful, please give me acces to the size
		_dex_t ng = round((real)S1.numCols()/4.0);

		// The return matrix, it starts gently nuh ?
		Matrix<complex> S(4*ng,4*ng);

		// We need an identity matrix, kindly
		Matrix<complex> ID(2*ng,2*ng);
		{
			ID.eye();
		}
		
		// We get all the submatrices copies we need
		// THIS IS SPILLAGE (or SPARTA, as your sensibility goes)
		
		Matrix<complex> S1_pm = S1.crop(0,2*ng,2*ng,2*ng);
		Matrix<complex> S1_pp = S1.crop(0, 0, 2 * ng, 2 * ng);
		Matrix<complex> S2_mp = S2.crop(2*ng,0,2*ng,2*ng);
		Matrix<complex> S2_pp = S2.crop(0, 0, 2 * ng, 2 * ng);
		Matrix<complex> S1_mp = S1.crop(2*ng,0,2*ng,2*ng);
		Matrix<complex> S1_mm = S1.crop(2 * ng, 2 * ng, 2 * ng, 2 * ng);
		Matrix<complex> S2_mm = S2.crop(2 * ng, 2 * ng, 2 * ng, 2 * ng);
		Matrix<complex> S2_pm = S2.crop(0, 2 * ng, 2 * ng, 2 * ng);

		Matrix<complex> I = ID - (S1_pm*S2_mp);
		Matrix<complex> G1 = I.inv()*S1_pp;

		// This one is fairly simple	
		Matrix<complex> S_pp = S2_pp*G1;

		// Ok this one is rookie business too
		Matrix<complex> S_mp = S1_mp + (S1_mm*S2_mp*G1);
		Matrix<complex> G2 = I.inv()*S1_pm;

		// The next one is really intricate
		Matrix<complex> S_pm = S2_pm + ((S2_pp*G2)*S2_mm);
		// But who the hell designed this damned matrix product
		Matrix<complex> S_mm = (S1_mm*(ID + (S2_mp*G2)))*S2_mm;

		// And here I present to you, the most unoptimized matrix concat !
		S = assemble(S_pp,S_pm,S_mp,S_mm);


		// Believe in the robustness of mkl_free at this one moment.
		return S;
	}




	/** \author Nicolas Roy */ 
	void MatrixBuilder::generateA(const GGrid & g_vectors, 
								  const WaveVector & wave, 
								  const Matrix<complex> & epsilon, 
								  const Matrix<complex> & iepsilon, 
								  Matrix<complex>& matrix_)
	{
		_dex_t ng = g_vectors.grid().length();
		if (!matrix_.hasContent()) matrix_.init(4 * ng, 4 * ng);
		matrix_.zeros();

		real mu0C = Math::mu0*Math::c;
		real k_norm = Math::twopi / wave.wavelength;
		
		complex ieo = Math::i / mu0C * k_norm;
		complex iseo = Math::i*(mu0C / k_norm);
		complex imo = Math::i*mu0C * k_norm;

		complex ismo = Math::i / (mu0C * Math::twopi / wave.wavelength);

		for (_dex_t g_j = 0; g_j < ng; g_j++) {
			Vec2<int> j = g2coord(g_j, g_vectors.getBoundaries(), g_vectors.grid().size());

			Vec2<_dex_t> i_j;
			i_j.x = j.x + g_vectors.getBoundaries().x;
			i_j.y = j.y + g_vectors.getBoundaries().y;

			complex ux_j = wave.getPlanarWaveVector().x + g_vectors.grid()(i_j.x, i_j.y).x;
			complex uy_j = wave.getPlanarWaveVector().y + g_vectors.grid()(i_j.x, i_j.y).y;

			
			for (int g_i = 0; g_i < ng; g_i++) {
				Vec2<int> i = g2coord(g_i, g_vectors.getBoundaries(), g_vectors.grid().size());

				Vec2<_dex_t> i_i;
				i_i.x = i.x + g_vectors.getBoundaries().x;
				i_i.y = i.y + g_vectors.getBoundaries().y;
				

				complex ux_i = wave.getPlanarWaveVector().x + g_vectors.grid()(i_i.x, i_i.y).x;
				complex uy_i = wave.getPlanarWaveVector().y + g_vectors.grid()(i_i.x, i_i.y).y;
				int dm1_ij = i.x - j.x;
				int dm2_ij = i.y - j.y;
				int i1_ij = dm1_ij + 2 * g_vectors.getBoundaries().x;
				int i2_ij = dm2_ij + 2 * g_vectors.getBoundaries().y;

				complex epsg = epsilon(i1_ij, i2_ij);
				complex epsinvg = iepsilon(i1_ij, i2_ij);

				if (g_i == g_j) {
					matrix_(2 * ng + g_i, g_j) = -ismo*ux_i*uy_j;
					matrix_(3 * ng + g_i, g_j) = -ismo*uy_i*uy_j; // TTT
					matrix_(2 * ng + g_i, ng + g_j) = ismo*ux_i*ux_j;
					matrix_(3 * ng + g_i, ng + g_j) = ismo*uy_i*ux_j; // TT
					matrix_(ng + g_i, 2 * ng + g_j) = -imo;
					matrix_(g_i, 3 * ng + g_j) = imo;
				}

				matrix_(3 * ng + g_i, g_j) = matrix_(3 * ng + g_i, g_j) + ieo*epsg;
				matrix_(2 * ng + g_i, ng + g_j) = matrix_(2 * ng + g_i, ng + g_j) - ieo*epsg;
				matrix_(g_i, 2 * ng + g_j) = iseo*ux_i*epsinvg*uy_j;
				matrix_(ng + g_i, 2 * ng + g_j) = matrix_(ng + g_i, 2 * ng + g_j) + iseo*uy_i*epsinvg*uy_j;
				matrix_(g_i, 3 * ng + g_j) = matrix_(g_i, 3 * ng + g_j) - iseo*ux_i*epsinvg*ux_j;
				matrix_(ng + g_i, 3 * ng + g_j) = -iseo*uy_i*epsinvg*ux_j;
			}
		}
	}
}