#pragma once

#include "Math.hpp"
#include "Matrix.hpp"
#include "WaveVector.hpp"


namespace Halcyon
{
	Vec2<int> g2coord(_dex_t g, Vec2<_dex_t> bounds, Vec2<_dex_t> dim);
	/**
	 * @brief Utilitary Class to create the different complex matrices.
	 * @details RCWA by Transfer Matrices is all about big nasty matrices.
	 * Here is a compilation of tools, some temporary to handle their creation/operation.
	 */
	class MatrixBuilder
	{
		public:
		MatrixBuilder() = delete;
		/**
		 * Matrices generators
		 * @brief These generators compute crucial base transfer matrices components.
		 * @todo Change this, this is bad c++, but our best option for now.
		 * @author Nicolas Roy
		 */
		static void generateU(const GGrid& g_vectors, const WaveVector& k, const complex& epsilon, Matrix<complex>& matrix_);
		static void generateVI(const GGrid& g_vectors, const WaveVector& k, const complex& epsilon, Matrix<complex>& matrix_);
		static void generateVE(const GGrid& g_vectors, const WaveVector& k, const complex& epsilon, Matrix<complex>& matrix_);
		static void generateA(const GGrid& g_vectors,
							  const WaveVector& k,
							  const Matrix<complex> & epsilon,
							  const Matrix<complex> & iepsilon,
							  Matrix<complex>& matrix_);

		static Matrix<complex> assemble(const Matrix<complex>& pp, const Matrix<complex>& pm, const Matrix<complex>& mp, const Matrix<complex>& mm);
		static Matrix<complex> generateS(const GGrid& g, const Matrix<complex>& T);
		static Matrix<complex> multS(Matrix<complex>& S1,Matrix<complex>& S2);
	};
}

