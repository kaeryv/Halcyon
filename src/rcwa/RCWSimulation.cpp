#include "RCWSimulation.hpp"



namespace Halcyon
{
    RCWSimulation::RCWSimulation(std::shared_ptr<JonesVector> p,
                                 std::shared_ptr<Spectrum> s,
                                 std::shared_ptr<Multilayer> m,
                                 std::shared_ptr<GGrid> cn,
                                 std::shared_ptr<OuterMediums> om) :
                   polarization(p),
                   spectrum(s),
                   multilayer(m),
                   conjNetwork(cn),
                   outerMedium(om)
    {}

    RCWSimulation::~RCWSimulation()
    {}

    void RCWSimulation::init()
    {
        Log::Info("Generating truncated reciprocal network.");
        
        /// Init buffer members
        {
            _dex_t ng = conjNetwork->grid().length();
            _j1plus.init(ng);
            _j1minus.init(ng);
            _j3plus.init(ng);
        }

        Log::Info("Processing islands Fourier transforms.");
        for (_dex_t l = 0; l < multilayer->size(); ++l) {
            (*multilayer)[l]->bake_islands(conjNetwork);
        }

        Log::Info("Building incident supervector.");

        Vec2<_dex_t> boundaries = conjNetwork->getBoundaries();
        Vec2<_dex_t> dimensions = conjNetwork->grid().size();

        _dex_t ng = conjNetwork->grid().length();
        _dex_t g0 = boundaries.x + dimensions.x * boundaries.y;

        _P_incident.init(4 * ng);
        _P_incident.zeros();

        _P_incident(g0) = polarization->first; // s-polar
        _P_incident(ng + g0) = polarization->second; // p-polar
    }

    void RCWSimulation::evaluate()
    {

        auto start = std::chrono::system_clock::now(); 


        for (_dex_t i = 0; i < spectrum->lambda.size(); i++) 
            for (_dex_t j = 0; j <  spectrum->theta.size(); j++) 
                for (_dex_t k = 0; k < spectrum->phi.size(); k++) 
                    this->iterate(i, j, k);
        
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
 
        std::cout << "finished computation at " << std::ctime(&end_time) << "elapsed time: " << elapsed_seconds.count() << "s\n";

        spectrum->save();
    }

    void RCWSimulation::extractCurrents(const WaveVector& k, _dex_t it)
    {		
        real sigma = conjNetwork->getUnitCellArea();
        real k_norm = 2 * Math::pi / spectrum->lambda[it];
        real q = sigma / (2 * Math::mu0*Math::c * k_norm );
        int ng = conjNetwork->grid().length();

        _j3plus.zeros();
        _j1plus.zeros();
        _j1minus.zeros();

        for (_dex_t g = 0; g < ng; g++) {
            const Matrix<complex>& kz_i = k.accessWaveVectorIncidentZ();
            const Matrix<complex>& kz_e = k.accessWaveVectorEmergentZ();

            vec2i m = conjNetwork->coord(g);
            m.x += conjNetwork->getBoundaries().x;
            m.y += conjNetwork->getBoundaries().y;

            if (std::abs(std::imag(kz_i(m.x, m.y)) < 1e-20))

            {
                complex a = q*kz_i(m.x, m.y);
                _j1plus(g) =  (complex)a*(pow(std::abs(_P_incident(g)), 2) + pow(std::abs(_P_incident(ng + g)),2));
                _j1minus(g) =  (complex)a*(pow(std::abs(_P_out(2 * ng + g)),2) + pow(std::abs(_P_out(3 * ng + g)),2));
            }
            
            if (std::abs(std::imag(outerMedium->second->epsilon(it))) > 0.0)
                _j3plus(g) = 0;
            else if (std::abs(std::imag(kz_e(m.x, m.y)))< 1e-20)
                _j3plus(g) = q*kz_e(m.x, m.y)*(std::pow(std::abs(_P_out(g)),2)+ std::pow(std::abs(_P_out(ng + g)),2));
        }
    }

    void RCWSimulation::iterate(_dex_t it, _dex_t it2, _dex_t it3)
    {
        real lambda = spectrum->lambda[it];
        real theta = spectrum->theta[it2];
        real phi = spectrum->phi[it3];

        std::pair<complex, complex> eps_outerMedium = 
                                    std::pair<complex, complex>
                                            (outerMedium->first->epsilon(it),
                                             outerMedium->second->epsilon(it));
        
        WaveVector k = WaveVector(*conjNetwork, 
                                  eps_outerMedium,  
                                   lambda, 
                                   theta,
                                   phi);

        totalSimTime = clock();

        std::cout <<std::setw(10) << "Computing wavelength :: " << std::setw(10) << spectrum->lambda[it]*pow(10, 9) << "nm |";
        
       
        

        MatrixBuilder::generateU (*conjNetwork, k, outerMedium->first->epsilon(it),  _U);
        
        MatrixBuilder::generateVI(*conjNetwork, k, outerMedium->first->epsilon(it),  _VI);
        
        MatrixBuilder::generateVE(*conjNetwork, k, outerMedium->second->epsilon(it), _VE);
        
        //Matrix<complex> T_interface = U*VE;
        Matrix<complex> S_interface = MatrixBuilder::generateS(*conjNetwork, _U*_VE);
        




#		pragma omp parallel for
        for (_dex_t l = 0; l < multilayer->size(); l++) 
        {
            (*multilayer)[l]->computeScatteringMatrix(*conjNetwork, k, _U, _VI, it);			
            std::cout << "=";
        }
        
        std::cout << "| Assembling matrices : ";


        _dex_t ng = conjNetwork->grid().length();


        Matrix<complex> S_layers(4*ng, 4*ng);
        {
            S_layers.eye();
        }
        
        for (_dex_t l = 0; l < multilayer->size(); ++l) {
            S_layers = MatrixBuilder::multS(S_layers, (*multilayer)[l]->scatteringMatrix());
        }
        
        // The total density matrix needs the interface.
        Matrix<complex> S_tot = MatrixBuilder::multS(S_layers, S_interface);
        _P_out = S_tot * _P_incident;

        this->extractCurrents(k,it);

        spectrum->R[it] = (_j1minus.sum() / _j1plus.sum()).real();
        spectrum->T[it] = (_j3plus.sum() /  _j1plus.sum()).real();

        std::cout << "Done !" << std::endl;
    }
}