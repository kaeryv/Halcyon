/**
 * @file
 * @brief Describes RCWSimulation
 */
#pragma once

#include "Log.hpp"
#include "Matrix.hpp"
#include "ThinMatrix.hpp"
#include "GGrid.hpp"
#include "WaveVector.hpp"
#include "Spectrum.hpp"

#include <memory>
#include <chrono>
#include <ctime>

namespace Halcyon
{
    // Let's rename those to some physical concepts
    typedef std::pair<complex, complex> JonesVector;
    typedef std::pair<std::shared_ptr<ConstantMaterial>, std::shared_ptr<Material>> OuterMediums;
    typedef std::vector<std::shared_ptr<Layer>> Multilayer;

    /**
     * @brief RCWA Algorithm container
     * @author Nicolas Roy
     * @details
     */
    class RCWSimulation
    {
        public:
        /**
         * @brief There is no defaults constructor
         */
            RCWSimulation() = delete;
        /**
         * @brief Main Constructor
         */
            RCWSimulation(std::shared_ptr<JonesVector> polar,
                          std::shared_ptr<Spectrum> spectrum,
                          std::shared_ptr<Multilayer> multilayer,
                          std::shared_ptr<GGrid> reciprocal_network,
                          std::shared_ptr<OuterMediums> ext_medium);
        /**
         * @brief Default Destructor
         */
            ~RCWSimulation();

        /**
         * @brief Initial operations
         */
            void init();
        
        /**
         * @brief Main algorithm
         */
            void evaluate();

        protected:
        /**
         * @brief One step of the RCWA algorithm
         */
            void iterate(_dex_t it, _dex_t it2, _dex_t it3);

        /**
         * @brief Compute electrodynamic currents from supervectors
         */
            void extractCurrents(const WaveVector& k, _dex_t l);

        
            clock_t	totalSimTime;                           ///< Simulation time for information
            _dex_t numIter;                                 ///< Current progress

            std::shared_ptr<Spectrum> spectrum;             ///< Spectrum
            std::shared_ptr<Multilayer> multilayer;         ///< The structure
            std::shared_ptr<GGrid> conjNetwork;             ///< Reciprocal lattice
            std::shared_ptr<OuterMediums> outerMedium;      ///< Incident and emergent mediums
            std::shared_ptr<JonesVector> polarization;      ///< Incident wave polarization

        private:
            ThinMatrix<complex> _P_incident;				///< Vector representing incident wave
            ThinMatrix<complex> _P_out;						///< Vector representing ougoing wave


            ThinMatrix<complex> _j1plus;                    ///< Buffers for ED currents values.
            ThinMatrix<complex> _j1minus;                   ///< Buffers for ED currents values.
            ThinMatrix<complex> _j3plus; 	                ///< Buffers for ED currents values.

            Matrix<complex> _U, _VI, _VE;					///< Buffers for transition matrices
    };
}