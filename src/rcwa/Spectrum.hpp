#pragma once

#include "mkl_config.h"

#include <vector>
#include <memory>
#include <fstream>


namespace Halcyon
{
    /**
     * @brief Spectral data container.
     * @author Nicolas Roy
     */
    class Spectrum {
        public:
            std::vector<real> lambda;   ///< Wavelength
            std::vector<real> theta;    ///< Azimuth
            std::vector<real> phi;      ///< Horizontal angle
            std::vector<real> R;        ///< Reflectivity
            std::vector<real> T;        ///< Transmission

            Spectrum(real lmn,
                     real lmx,
                     _dex_t nl,
                     real tmn,
                     real tmx,
                     _dex_t nt,
                     real pmn,
                     real pmx,
                     _dex_t np)
            {
                if(nl==1)
                    lambda.push_back(lmn);
                else
                {
                    real dlambda = (lmx - lmn)/(nl-1);
                    lambda.assign(nl, 0.0);
                    for (_dex_t i = 0; i < nl; ++i)
                        lambda[i] = lmn + dlambda * i;
                }


                if(nt == 1)
                    theta.push_back(tmn);
                else
                {
                    real dtheta = (tmx - tmn)/(nt-1);
                    theta.assign(nt, 0.0);

                    for (_dex_t i = 0; i < nt; ++i)
                        theta[i] = tmn + dtheta * i;
                }

                if(np == 1)
                    phi.push_back(pmn);
                else
                {
                    real dphi = (pmx - pmn)/(np-1);
                    phi.assign(np, 0.0);

                    for (_dex_t i = 0; i < np; ++i)
                        phi[i] = pmn + dphi * i;
                }
                R.assign(nl, 0.0);
                T.assign(nl, 0.0);
            }

            void save() {
                std::ofstream file = std::ofstream("R.txt", std::ios::out);
                for (_dex_t i = 0; i < R.size(); i++) {
                    file << R[i] << std::endl;;
                }
                file.close();
                file = std::ofstream("T.txt", std::ios::out);

                for (_dex_t i = 0; i < T.size(); i++) {
                    file << T[i] << std::endl;;
                }
                file.close();
                file = std::ofstream("lambda.txt", std::ios::out);
                for (_dex_t i = 0; i < lambda.size(); i++) {
                    file << lambda[i] << std::endl;;
                }
                file.close();
            }
        };
}