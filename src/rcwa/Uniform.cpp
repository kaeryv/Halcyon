#include "Uniform.hpp"


namespace Halcyon
{
	Uniform::Uniform():
		Island()
	{

	}

    Uniform::Uniform(std::shared_ptr<Material> mat) :
	    Island(mat)
	{

	}
	void Uniform::compute_transform(const GGrid& g_vectors)
	{
		for (_dex_t i = 0; i < g_vectors.exgrid().numRows(); i++)
            for (_dex_t j = 0; j < g_vectors.exgrid().numCols(); j++)
			{
				_fTransform(i,j) = 0.0;
			}
	}

	Uniform::~Uniform()
	{

	}
}