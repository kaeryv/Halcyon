#pragma once

#include "Island.hpp"
#include "GGrid.hpp"


namespace Halcyon
{
	class Uniform : public Island
	{
		public:
			Uniform();
			Uniform(std::shared_ptr<Material> mat);
			void compute_transform(const GGrid& g_vectors) override;
			~Uniform() override;
	};
}
