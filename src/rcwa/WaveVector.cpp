#include "WaveVector.hpp"

namespace Halcyon
{
	WaveVector::WaveVector(const GGrid & g, const std::pair<complex, complex>& external_medium, const real lambda, const real theta, const real phi)
	{
		wavelength = lambda;
		this->computeWaveVector(g, theta, phi, external_medium);
		
		this->computePolarisationBase(g, external_medium);
		
	}	

	void WaveVector::computeWaveVector(const GGrid& g, const real theta, const real phi, const std::pair<complex, complex>& medium)
	{
		// We compute the invariant planar components of the wave vector in the incident medium
		this->computePlanarWaveVector(theta, phi, medium.first);

		// Then we compute the wave vector component along z in both mediums.
		_waveVectorZI.init(g.grid().numRows(), g.grid().numCols());
		_waveVectorZI = this->computeWaveVectorAlongZ(g, medium.first);
		_waveVectorZE.init(g.grid().numRows(), g.grid().numCols());
		_waveVectorZE = this->computeWaveVectorAlongZ(g, medium.second);
	}

	void WaveVector::computePolarisationBase(const GGrid& g, const std::pair<complex, complex>& medium)
	{

		_eta.first.init(g.grid().numRows(), g.grid().numCols());
		_eta.second.init(g.grid().numRows(), g.grid().numCols());
		_eta = this->computePolarizationBaseEta(g);
		
		_mu_emergent.first.init(g.grid().numRows(), g.grid().numCols());
		_mu_emergent.second.init(g.grid().numRows(), g.grid().numCols());
		
		_mu_emergent = this->computePolarizationBaseMu(g, medium.second, _waveVectorZE);
		
		_mu_incident.first.init(g.grid().numRows(), g.grid().numCols());
		_mu_incident.second.init(g.grid().numRows(), g.grid().numCols());
		_mu_incident = this->computePolarizationBaseMu(g, medium.first, _waveVectorZI);
	}



	void WaveVector::computePlanarWaveVector(const real theta, const real phi, const complex& epsilon_incident)
	{
		real theta_r = Math::toRadians(theta);
		real phi_r = Math::toRadians(phi);

		_waveVectorXY.x = ((pow(epsilon_incident, 0.5)*Math::twopi / wavelength)*sin(theta_r)*cos(phi_r));
		_waveVectorXY.y = (((pow(epsilon_incident, 0.5)*Math::twopi) / wavelength)*sin(theta_r)*sin(phi_r));
	}

	ComplexBase WaveVector::computePolarizationBaseEta(const GGrid& g)
	{
		ComplexBase eta;
		eta.first.init(g.grid().numRows(), g.grid().numCols());
		eta.second.init(g.grid().numRows(), g.grid().numCols());

		vec3c ez = { 0.0, 0.0, 1.0 };

		
		for (_dex_t i = 0; i < g.grid().numRows(); i++) 
		for (_dex_t j = 0; j < g.grid().numCols(); j++) 

		{
			vec3c u;
			{
				u.x = _waveVectorXY.x + g.grid()(i,j).x;
				u.y = _waveVectorXY.y + g.grid()(i,j).y;
				u.z =  0.0;
			}
			real uNorm2 = std::pow(u.x.real(), 2.0) + std::pow(u.y.real(), 2.0);
			real uNorm = std::pow(uNorm2, 0.5);

			if (uNorm2 > 1e-20) {
				u.x /= uNorm;
				u.y /= uNorm;
			}
			else {
				u.x = 1.0;
				u.y = 0.0;
			}
			vec3c temp_eta = Math::cross(u, ez);

			eta.first(i,j) = temp_eta.x;
			eta.second(i,j) = temp_eta.y;

		}
		return eta;
	}

	ComplexBase WaveVector::computePolarizationBaseMu(const GGrid& g, const complex& epsilon, const Matrix<complex>& kz)
	{
		ComplexBase mug;
		mug.first.init(g.grid().numRows(), g.grid().numCols());
		mug.second.init(g.grid().numRows(), g.grid().numCols());

		for (_dex_t i = 0; i < g.grid().numRows(); ++i) 
			for (_dex_t j = 0; j < g.grid().numCols(); ++j)
			{ 
				
					Vec2<complex> u = { _waveVectorXY.x + g.grid()(i,j).x,_waveVectorXY.y + g.grid()(i,j).y};
					
					real uNorm2 = pow(u.x.real(), 2.0) + pow(u.y.real(), 2.0);
					real uNorm = pow(uNorm2, 0.5);
					if (uNorm2 > 1e-20) {
						u.x /= uNorm;
						u.y /= uNorm;
				
					}
					else {
						u.x = { 1.0, 0.0 };
						u.y = { 0.0, 0.0 };
					
					}
					mug.first(i,j) = (kz(i,j) / (sqrt(epsilon) * Math::twopi / wavelength)*u.x);
					mug.second(i,j) = (kz(i,j) / (sqrt(epsilon) * Math::twopi / wavelength)*u.y);
					
				}
		return mug;
	}

	Matrix<complex> WaveVector::computeWaveVectorAlongZ(const GGrid& g, const complex& epsilon)
	{
		Matrix<complex> k_z_g(g.grid().numRows(), g.grid().numCols());
		Vec2<complex> u;
		real uNorm2;

		for (_dex_t i = 0; i < g.grid().numRows(); i++) 
		for (_dex_t j = 0; j < g.grid().numCols(); j++) {
			u.x = _waveVectorXY.x + g.grid()(i,j).x;
			u.y = _waveVectorXY.y + g.grid()(i,j).y;
			
			uNorm2 = std::pow(u.x.real(), 2) + std::pow(u.y.real(), 2);
			complex kz = sqrt(epsilon*pow((Math::twopi / wavelength), 2) - uNorm2);
			if (kz.imag()<0)
				kz = -kz;
			else if (std::abs(kz.imag())<1e-20) {
				if (kz.real()<0)kz = -kz;
			}
			k_z_g(i,j) = kz;
		}
		return k_z_g;
	}
}