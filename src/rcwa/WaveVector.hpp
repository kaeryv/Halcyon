/**
 * \file
 * \brief Describes WaveVector Class
 */
#pragma once

#include "mkl_config.h"
#include "Math.hpp"
#include "GGrid.hpp"
#include "Matrix.hpp"
#include "Log.hpp"


#include <cmath>
#include <fstream>
#include <array>
#include <iostream>
#include <utility>
#include <numeric>



namespace Halcyon
{
	/**
	 * We typedef this quite complex data structure.
	 * Bidimentional frequency domain representation base.
	 * By the straightforward typename ComplexBase
	 */
	typedef std::pair<Matrix<complex>, Matrix<complex>> ComplexBase;

	class WaveVector
	{
		public:
			WaveVector(const GGrid& g, const std::pair<complex, complex>& external_medium, const real lambda, const real theta, const real phi);
			~WaveVector(){}

			void computePolarisationBase(const GGrid& g_vectors, const std::pair<complex, complex>& medium);
			void computeWaveVector(const GGrid& g, const real theta, const real phi, const std::pair<complex, complex>& medium);


			const ComplexBase& accessPolarizationBaseEta() const { return _eta; }
			const ComplexBase& accessPolarizationBaseMuIncident() const { return _mu_incident; }
			const ComplexBase& accessPolarizationBaseMuEmergent() const { return _mu_emergent; }

			const Matrix<complex>& accessWaveVectorIncidentZ() const { return _waveVectorZI; }
			const Matrix<complex>& accessWaveVectorEmergentZ() const { return _waveVectorZE; }
			
			const vec2c& getPlanarWaveVector() const { return _waveVectorXY; }

			real wavelength;
		private:
			/**
			 * \brief Tools for computation of the polarization base.
			 * \author Nicolas Roy
			 */
			ComplexBase computePolarizationBaseEta(const GGrid& g);
			ComplexBase computePolarizationBaseMu(const GGrid& g, const complex& epsilon, const Matrix<complex>& kz);

			/**
			 * \brief Tools for computation of the wave vector along z (propagation) axis.
			 * \author Nicolas Roy
			 */
			Matrix<complex> computeWaveVectorAlongZ(const GGrid& g, const complex& epsilon);

			/**
			 * \brief Deferred computation of the wave vector in the slab plane.
			 * \author Nicolas Roy
			 */
			void computePlanarWaveVector(const real theta, const real phi, const complex& epsilon_incident);

		private:

			Matrix<complex> _waveVectorZI;
			Matrix<complex> _waveVectorZE;
			vec2c _waveVectorXY;
			ComplexBase _eta, _mu_incident, _mu_emergent;
	};
}