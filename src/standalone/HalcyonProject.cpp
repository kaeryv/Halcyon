#include "HalcyonProject.hpp"

namespace Halcyon
{
    HalcyonProject::HalcyonProject(std::string path_incident, std::string path_structure)
    {
        Material::LoadMaterialsDataBase("../Halcyon/materials/database.json");

        SequencialInputParser store(path_incident, path_structure);
        this->load(store);

        
        // Inputstorage container is now destroyed.
    }
    void HalcyonProject::load(InputParser& input)
    {
        
        polarization = input.storage->polarization;
        std::cout << input.storage->nLambda << std::endl;
        
        // Generating spectrum
        spectrum = std::make_shared<Spectrum>(input.storage->lambdaMin,
                                              input.storage->lambdaMax,
                                              input.storage->nLambda,
                                              input.storage->thetaMin,
                                              input.storage->thetaMax,
                                              input.storage->nTheta,
                                              input.storage->phiMin,
                                              input.storage->phiMax,
                                              input.storage->nPhi);

        // Generating required materials
        
        for(std::pair<std::string, std::shared_ptr<Material>> pair : input.storage->materials)
        {
            std::shared_ptr<Material> this_material = pair.second;
            //std::cout << " material name " << pair.first << std::endl;
            this_material->init(spectrum->lambda);
        }

        // Sharing reference to Multilayer
        multilayer = input.storage->multilayer;

        // Generating the reciprocal lattice
        conjNetwork = std::make_shared<GGrid>(input.storage->params);
        std::cout << conjNetwork->getExternalBoundaries().x;
        // Sharing reference to Outer Mediums
        outerMedium = input.storage->outerMediums;
    }

    void HalcyonProject::process(_dex_t max_omp_cores)
    {
        simulation = std::make_unique<RCWSimulation>(polarization,
                                                     spectrum,
                                                     multilayer,
                                                     conjNetwork,
                                                     outerMedium);
        simulation->init();
        simulation->evaluate();
    }
}