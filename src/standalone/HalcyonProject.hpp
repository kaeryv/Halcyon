/**
 * @file
 * @brief Describes HalcyonProject
 */

#pragma once

#include "Log.hpp"
#include "Spectrum.hpp"
#include "InputParser.hpp"
#include "RCWSimulation.hpp"

#include <memory>


namespace Halcyon
{
    /**
     * @author Nicolas Roy
     * @brief  Class setting-up Halcyon RCWSimulation from resources.
     * @todo   Finish implementation.
     */
    class HalcyonProject
    {
        public:
            HalcyonProject(){Log::Fatal("Not implemented.");}
            HalcyonProject(std::string path){Log::Fatal("Not implemented.");}
            HalcyonProject(std::string path_incident, std::string path_structure);

        /**
         * @brief Performs simulation
         * @todo Actually just runs RCWSilumation
         */
            void process(_dex_t max_omp_cores=1);
        /**
         * @brief Loads project from project file.
         * @todo Not implemented.
         */
            void load(std::string path){Log::Fatal("Not implemented.");};
        /**
         * @brief Saves project to project file.
         * @todo Not implemented.
         */
            void save(std::string path){Log::Fatal("Not implemented.");};
        protected:
            void load(InputParser& input);

            std::unique_ptr<RCWSimulation> simulation;

            std::shared_ptr<JonesVector> polarization;
            std::shared_ptr<Spectrum> spectrum;
            std::shared_ptr<Multilayer> multilayer;
            std::shared_ptr<GGrid> conjNetwork;
            std::shared_ptr<OuterMediums> outerMedium;
    };
}