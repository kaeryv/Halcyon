/**
 * \file
 * \brief   Standalone Halcyon RCWA software
 * \details  Yep to write
 */


#include "Log.hpp"
#include "Matrix.hpp"
#include "ThinMatrix.hpp"
#include "ArgumentsExcavator.hpp"
#include "HalcyonProject.hpp"
#include "PythonMonolith.hpp"
#include "mkl_config.h"


void banner();


/**
 * \brief  Entry-point.
 * \author Nicolas Roy
 */
int main(int argc, char** argv) 
{
    
    banner();
    
    Log::SetLogLevel(Log::Level::LevelInfo);
    ArgumentsExcavator argx;

    argx.requestArgument("Structure input file", 
                         "This structure file describes the different layers of the stack and their islands.", 
                         "struct", 
                         ArgumentsExcavator::Argument::Type::String,
                         ArgumentsExcavator::Argument::Priority::Required);

    argx.requestArgument("Incident input file", 
                           "This incident file describes the characteritics of the plane wave inciding on the multilayer.", 
                         "inc", 
                         ArgumentsExcavator::Argument::Type::String,
                         ArgumentsExcavator::Argument::Priority::Required);

    argx.requestArgument("Number of openMP cores", 
                         "The number of openMP cores to allocate for parallel computing section.", 
                         "cores", 
                         ArgumentsExcavator::Argument::Type::Integer,
                         ArgumentsExcavator::Argument::Priority::Optional);


    argx.crunchInput(argc, argv);



    // Setting-up python environment

    PythonMonolith::Singleton()->import("sys");
    PythonMonolith::Singleton()->import("Halcyon");
    PythonMonolith::Singleton()->import("PyPreprocessors");



    Halcyon::HalcyonProject project(argx.getStringArg("inc"), argx.getStringArg("struct"));

    project.process();
}


void banner()
{
    std::cout << "██╗  ██╗ █████╗ ██╗      ██████╗██╗   ██╗ ██████╗ ███╗   ██╗" << std::endl;
    std::cout << "██║  ██║██╔══██╗██║     ██╔════╝╚██╗ ██╔╝██╔═══██╗████╗  ██║" << std::endl;
    std::cout << "███████║███████║██║     ██║      ╚████╔╝ ██║   ██║██╔██╗ ██║" << std::endl;
    std::cout << "██╔══██║██╔══██║██║     ██║       ╚██╔╝  ██║   ██║██║╚██╗██║" << std::endl;
    std::cout << "██║  ██║██║  ██║███████╗╚██████╗   ██║   ╚██████╔╝██║ ╚████║" << std::endl;
    std::cout << "╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═══╝" << std::endl;
    std::cout << "--- Rigorous Coupled Wave Analysis Software UNamur 2018 ----" << std::endl;
}