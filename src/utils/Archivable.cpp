#include "Archivable.hpp"


/** \author Nicolas Roy */ 
void Archivable::dump()
{
    std::ofstream o("./io/" + _archive_name + ".arch.json");
	if(!o.is_open())
		Log::Error("Failed to write archive JSON");
        
	o << std::setw(4) << output << std::endl;
	o.close();
}

/** \author Nicolas Roy */ 
void Archivable::setArchiveName(std::string name)
{
    _archive_name = name;
}

/** \author Nicolas Roy */ 
json& Archivable::asJson()
{
    return _archive;
}
  
