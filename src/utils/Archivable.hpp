#pragma once

#include "json.hpp"

#include <fstream>

using namespace nlohmann;

class Archivable
{
    public:
        void dump();
        void setArchiveName(std::string name);
        virtual void archive() = 0;
        json& asJson();
    protected:
        json _archive;
        std::string _archive_name;
};