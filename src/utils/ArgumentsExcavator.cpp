#include "ArgumentsExcavator.hpp"

#include <map>


/** \author Nicolas Roy */ 
int ArgumentsExcavator::getIntegerArg(std::string name)
{
    // NOT IMPLEMENTED
    return 0;
}

/** \author Nicolas Roy */ 
double ArgumentsExcavator::getDoubleArg(std::string name)
{
    // NOT IMPLEMENTED
    return 0.0;
}

/** \author Nicolas Roy */ 
std::string ArgumentsExcavator::getStringArg(std::string name)
{
    if(arguments.find(name) != arguments.end())
        if(arguments[name].wasCrunched)
            return arguments[name].value;
        else
            Log::Fatal("Argument was not set.");
    else
        Log::Fatal("Argument " + name + " does not exist.");
}

/** \author Nicolas Roy */ 
void ArgumentsExcavator::requestArgument(
                        std::string name, 
                        std::string description, 
                        std::string identifier, 
                        Argument::Type type, 
                        Argument::Priority priority
                    )
{
    Argument arg;
    arg.name = name;
    arg.description = description;
    arg.description = description;
    arg.type = type;
    arg.priority = priority;

    arguments[identifier] = arg;
}

/** \author Nicolas Roy */            
void ArgumentsExcavator::crunchInput(int argc, char** argv)
{
    bool waiting_for_explicit_argument = false;

    std::pair<std::string, Argument*> args_tuple;

    for(int argi = 1; argi < argc; ++argi)
    {
        std::string raw_arg = argv[argi];
        
        if(!waiting_for_explicit_argument)
        {
            if(raw_arg.substr(0,1) == "-")
            {
                // We extract the identifier
                args_tuple.first = raw_arg.substr(1,raw_arg.size()-1);

                // We look for it in the db
                std::map<std::string, Argument>::iterator it = arguments.find(args_tuple.first);
                
                // If we find the identifier, we get a pointer to the value.
                if(it != arguments.end())
                {
                    //element found;
                    args_tuple.second = &it->second;
                    Log::Info(args_tuple.second->name + " was found in input.");

                    if(args_tuple.second->type == Argument::Type::Logical)
                    {
                        waiting_for_explicit_argument = false;
                        args_tuple.second->value = "true";
                        args_tuple.second->wasCrunched = true;
                        Log::Info("Collected value " + args_tuple.second->value + " for " +args_tuple.first);
                    }
                    else
                    {
                        waiting_for_explicit_argument = true;
                    }
                }
                else
                {
                    Log::Error("Unrecognized junk argument: " + raw_arg);
                }
            }
            else
            {
                
                Log::Error("Unknown stinky expression: " + raw_arg);
            }
        }
        else
        {
            // We are waiting for explicit data, do we have a slot ready ?
            if(args_tuple.second != nullptr)
            {
                args_tuple.second->value = raw_arg;
                args_tuple.second->wasCrunched = true;
                Log::Info("Collected value " + raw_arg + " for " +args_tuple.first);
            }
            else
            {
                Log::Error("Expected explicit parameter value for " + args_tuple.first);
            }
             waiting_for_explicit_argument = false;
        }
    }

    if(waiting_for_explicit_argument)
    {
        Log::Error("Still waiting for argument declaration.");
    }

    for(auto& arg : arguments)
    {
        if(arg.second.wasCrunched != true && arg.second.priority == Argument::Priority::Required)
        {
            Log::Error("Argument " + arg.second.name + " was not specified but is required.");
        }
    }
}
