#pragma once

#include <map>
#include <iterator>

#include "Log.hpp"

class ArgumentsExcavator
{
    public:
    struct Argument
    {
        enum class Type
        {
            None = 0, Integer, Float, String, Logical
        }
        type;

        enum class Priority
        {
            Optional = 0, Required
        }
        priority;

        std::string name;
        std::string description;
        std::string identifier;
        std::string value;
        bool wasCrunched = false;
    };

    int getIntegerArg(std::string name);
    double getDoubleArg(std::string name);
    std::string getStringArg(std::string name);

    void requestArgument(std::string name, 
                         std::string description, 
                         std::string identifier, 
                         Argument::Type type, 
                         Argument::Priority priority);

    void crunchInput(int argc, char** argv);
    protected:
        std::map<std::string, Argument> arguments;
};