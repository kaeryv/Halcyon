#include "Log.hpp"

Log::Level Log::m_LogLevel = Log::LevelWarning;
bool Log::m_QueryOn = true;
std::string Log::m_LogCache = std::string();

void Log::Info(std::string s)
{
	
	if (m_LogLevel >= LevelInfo)
		std::cout << "[INFO] " << s << std::endl;
		m_LogCache +=(s+"\n");
}

void Log::Warning(std::string s)
{
	if (m_LogLevel >= LevelWarning)
		std::cout << "[WARNING] " << s << std::endl;
		m_LogCache +=(s+"\n");
}

void Log::Error(std::string s)
{
	if (m_LogLevel >= LevelError)
	{
		std::cout << "[ERROR] " << s << std::endl;
		m_LogCache +=(s+"\n");
		std::cin.get();
	}
}
void Log::Fatal(std::string s)
{
	if (m_LogLevel >= LevelError)
	{
		std::cout << "[FATAL] " << s << std::endl;
		m_LogCache +=(s+"\n");
		Log::Dump("crash.log");
		exit(-2);
	}
}

void Log::Query(std::string s)
{
	if (m_LogLevel >= LevelWarning)
	{
		std::cout << "[QUERY] " << s << std::endl;
		if(m_QueryOn)
			std::cin.get();
	}

}

void Log::Dump(std::string output_filename)
{
	std::ofstream o(output_filename);

	if(o.is_open())
	{
		o << m_LogCache;
	}
	else
	{
		Log::Error("Failed to open log output file.");
	}
}

void Log::Hbar()
{
	std::cout << "---------------<>------------~~<>~~------------<>---------------" << std::endl;
}
