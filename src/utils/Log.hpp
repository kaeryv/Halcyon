#pragma once

#include <iostream>
#include <fstream>
#include <string>


class Log
{
public:
	enum Level : char 
	{
		LevelError = 0, LevelWarning, LevelInfo
	};
public:
	Log() = delete;
	static void SetLogLevel(Level level) { m_LogLevel = level; }
	static void YesToAll() { m_QueryOn = false; }
	static void Info(std::string s);
	static void Warning(std::string s);
	static void Error(std::string s);
	static void Fatal(std::string s);
	static void Query(std::string s);
    static void Hbar();
	static void Dump(std::string output_filename);
private:
	static Level m_LogLevel;
	static bool m_QueryOn;
	static std::string m_LogCache;
};

