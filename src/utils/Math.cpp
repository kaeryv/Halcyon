#include "Math.hpp"

namespace Math {
	real toRadians(real d)
	{
		return d / (real)180.0*pi;
	}

	Vec3<complex> cross(const Vec3<complex>& a, const Vec3<complex>& b)
	{
		Vec3<complex> c = { 0,0, 0};
		c.x = a.y * b.z - b.y * a.z;
		c.y = -(a.x * b.z - a.z * b.x);
		return c;
	}
}