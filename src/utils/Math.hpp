#pragma once

#include "mkl_config.h"
#include "VectorUtils.hpp"


namespace Math 
{
	constexpr real pi = 3.141592653589793;
	constexpr real twopi = 2.0 * pi;
	constexpr complex i = { 0.0, 1.0 };
	constexpr real mu0 = 4.0*pi *1e-7;
	constexpr real c = 299792458.0;
	real toRadians(real d);
	Vec3<complex> cross(const Vec3<complex>& a, const Vec3<complex>& b);
}
