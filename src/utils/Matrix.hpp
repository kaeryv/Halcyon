/**
 * @file 
 * @brief   Describes Matrix Template Class
 * @details This Matrix class implements very basic linear algebra features.
*/

#pragma once

#include "VectorUtils.hpp"
#include "MatrixContainer.hpp"
#include "Log.hpp"

#include <memory>
#include <iomanip>

constexpr complex BLAS_ALPHA = 1.0;
constexpr complex BLAS_BETA  = 0.0;


namespace Halcyon
{
    /**
     * @brief Template Class Matrix
     * @author Nicolas Roy
     * @details
     */
    template <typename Type>
    class Matrix
    {
    public:
    /**
     * @brief Default Constructor
     */
        Matrix() : _data(nullptr), _rows(0), _cols(0)
        {}

    /**
     * @brief Copy (Clone) Constructor
     * @details This is called on function return, so we only copy the structure and take
     * ownership of the data.
     */
        Matrix(const Matrix& mat)
        {
            _data = mat._data;
            _name = mat._name;
        }

    /**
     * @brief Named Matrix Constructor
     */
        Matrix(std::string n, unsigned r, unsigned c) : Matrix(r, c)
        {
            _name = n;
        }

    /**
     * @brief Anonymous Matrix Constructor
     */
        Matrix(unsigned rows, unsigned cols) : Matrix()
        {
            this->init(rows, cols);
             _name = "Unnamed matrix";
        }
        /**
         * @brief Performs deep copy of the Matrix data to a new one.
         */
        Matrix copy() const
        {
            // We init a similar Matrix
            Matrix destination(this->_rows, this->_cols);

            // We deep copy the whole data container
            destination._data->copy((*this->_data));

            return destination;
        }

        Matrix clone()
        {
            // We init a empty Matrix
            Matrix lhs;
            // We give her license to our data container.
            lhs._data = (std::shared_ptr<MatrixContainer<Type>>)this->data;
            return lhs;
        }

    /**
     * @brief Default Destructor
     * @details Note that at this point, the ownership of the Matrix is lost on MatrixContainer
     * Container will be destroyed if there is no more owner.
     */
        ~Matrix()
        {}

    /**
     * @brief Generate new data container for the Matrix.
     * @pre The old one will be trashed
     */
        void init(_dex_t rows, _dex_t cols);

    /**
     * @brief Free the matrix memory
     * @pre Only Method allowed to do so.
     */
        void free();

    public:
    /**
     * @brief Check if Matrix poces ownershipt to a content
     */
        bool hasContent()
        {
            return _data != nullptr;
        }
    /**
     * @brief Display the Matrix in the terminal.
     * @todo Use something cooler to do this.
     */
        void print()
        {
            std::cout << "mat " << _name << " " << _rows << " by " << _cols << " =" << std::endl;
            //std::cout << std::setprecision(3) << std::endl;

            for(_dex_t i = 0; i < _rows; ++i)
            {
                std::cout << " |";
                for(_dex_t j = 0; j < _cols; ++j)
                {
                    std::cout << " " << (*this)(i,j);
                }
                std::cout << " |" << std::endl;
            }
        }

    /**
     * @brief Indentity Matrix
     */
        void eye();
    /**
     * @brief Fills Matrix with zeros
     */
        void zeros();
    /**
     * @brief Fills Matrix with ones
     */
        void ones();
    /**
     * @brief Scales Matrix by real constant
     * @param s Real Constant
     */
        void scale(Type s);
    /**
     * @brief Fills Matrix with real constant.
     * @param s Real Constant
     */
        void fill(real s);

    /**
     * @brief Addition Between Two Matrix
     * @param rhs Righ hand Side of the sum
     * @return The operation returns a new Matrix C = A+B by cloning.
     * @details The data allocated inside the scope of the operator is not copied.
     * It's owernership is shared with return Matrix by copy constructor.
     * The return Matrix ends up taking full ownership when the scope owener gets destroyed.
     */
        Matrix operator+(const Matrix& rhs) const;
    /**
     * @brief Substraction Between Two Matrix
     * @param rhs Righ hand Side of the substraction
     * @return The operation returns a new Matrix C = A-B by cloning.
     * @details The data allocated inside the scope of the operator is not copied.
     * It's owernership is shared with return Matrix by copy constructor.
     * The return Matrix ends up taking full ownership when the scope owener gets destroyed.
     */
        Matrix operator-(const Matrix& rhs) const;
    /**
     * @brief Product Between Two Matrix
     * @param rhs Righ hand Side of the multiplication
     * @return The operation returns a new Matrix C = AB by cloning.
     * The operation is performed by HP external library.
     * @details The data allocated inside the scope of the operator is not copied.
     * It's owernership is shared with return Matrix by copy constructor.
     * The return Matrix ends up taking full ownership when the scope owener gets destroyed.
     */
        Matrix operator*(const Matrix& rhs) const;

    /**
     * @brief Asignment of Matrix
     * @param rhs Righ hand Side of the equal sign
     * @return The operation returns the actual matrix by reference.
     * @details The rhs is deep-copied into this Matrix.
     */
        Matrix& operator=(const Matrix& rhs);

    /**
     * @brief Asignment of Matrix by addition
     * @param rhs Righ hand Side of the equal sign
     * @return The operation returns the actual matrix by reference.
     * @details The rhs is added into this Matrix.
     */
        Matrix& operator+=(const Matrix& rhs);

    /**
     * @brief Asignment of Matrix by addition
     * @param rhs Righ hand Side of the equal sign
     * @return The operation returns the actual matrix by reference.
     * @details The rhs is added into this Matrix.
     */
        Matrix& operator-=(const Matrix& rhs);
    /**
     * @brief Asignment of Matrix by substraction
     * @param rhs Righ hand Side of the equal sign
     * @return The operation returns the actual matrix by reference.
     * @details The rhs is substracted from this Matrix.
     */
        Matrix& operator*=(const Matrix& rhs);
    /**
     * @brief Asignment of Matrix by division
     * @param rhs Righ hand Side of the equal sign
     * @return The operation returns the actual matrix by reference.
     * @details The rhs is divided term-by-term from this Matrix.
     */
        Matrix& operator/=(const Matrix& rhs);


    /**
     * @brief Matrix-Scalar assignement
     * @param s scalar
     */
        Matrix& operator=(const Type s);

    /**
     * @brief Matrix-Scalar assignment-addition
     * @param s scalar
     */
        Matrix& operator+=(const Type s);

    /**
     * @brief Matrix-Scalar assignment-substraction
     * @param s scalar
     */
        Matrix& operator-=(const Type s);

    /**
     * @brief Matrix-Scalar assignment-multiplication
     * @param s scalar
     */
        Matrix& operator*=(const Type s);

    /**
     * @brief Matrix-Scalar assignment-division
     * @param s scalar
     */
        Matrix& operator/=(const Type s);

    /**
     * @brief Matrix-Scalar multiplication
     * @param s scalar
     */
        Matrix operator * (const Type) const;

    /**
     * @brief Matrix-Scalar division
     * @param s scalar
     */
        Matrix operator / (const Type) const;

    /**
     * @brief Matrix-Scalar addition
     * @param s scalar
     */
        Matrix operator + (const Type) const;

    /**
     * @brief Matrix-Scalar substraction
     * @param s scalar
     */
        Matrix operator - (const Type) const;

        /// Operations
        Type sum()
        {
            Type ret = 0.0;;
            for (_dex_t i = 0; i < length(); ++i) {
                ret += (*_data)[i];
            }
            return ret;
        }
    /**
     * @brief Matrix Inverse
     * @return new matrix
     */
        Matrix inv() const;

    /**
     * @brief Pade-Exponent matrix exponential
     * @return new matrix
     */
        Matrix exp() const;

    /**
     * @brief Experimental sub - Matrix creator.
     */
        Matrix crop(_dex_t row, _dex_t col, _dex_t nrows, _dex_t ncols) const;

 

        // Useful public attributes
        Vec2<_dex_t> size() const { return { this->_rows, this->_cols }; }
        _dex_t length() const { return _data->length(); }

        _dex_t numRows() const { return this->_rows; }
        _dex_t numCols() const { return this->_cols; }



        // Others
        static void memStat();


    /**
     * @brief Matlab-Fortran Style Matrix element acces but Row Major
     * @param row the first index is row index
     * @param col the fast index is col index (row-major)
     * @return Reference to the element.
     */
        Type& operator()(const _dex_t& row, const _dex_t& col)
        {
            return (*this->_data)[row*_cols + col];
        }
    /**
     * @brief Matlab-Fortran Style Matrix element acces but Row Major
     * @param row the first index is row index
     * @param col the fast index is col index (row-major)
     * @return Constant Reference to the element.
     */
        const Type& operator() (const _dex_t& row, const _dex_t& col) const
        {
            return (*this->_data)[row*_cols + col];
        }

        Type& operator [] (const _dex_t index)
        {
            return (*this->_data)[index];
        }

        const Type& operator [] (const _dex_t index) const
        {
            return (*this->_data)[index];
        }

    public:
        // For display
        std::string _name;

        _dex_t _rows, _cols;  ///< Matrix size

        // Matrix data container
        std::shared_ptr<MatrixContainer<Type>> _data;

        Type* begin()
        {
            return _data->begin();
        }
        Type* begin() const
        {
            return _data->begin();
        }
    };

    /**
     * @todo remove this bad shit
     */
    template<typename Type>
    void imp_multiply(const Matrix<Type>& A, const Matrix<Type>& B, Matrix<Type>& C);
}

// Include the implementation of class
#include "Matrix.tpp"

