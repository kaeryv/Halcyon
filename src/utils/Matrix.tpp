namespace Halcyon
{

    template<typename Type>
    inline void Matrix<Type>::init(_dex_t rows, _dex_t cols)
    {
        _rows = rows; _cols = cols; 
        _dex_t length = rows * cols;
        _data = std::make_shared<MatrixContainer<Type>>(length);
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::crop(_dex_t row, _dex_t col, _dex_t nrows, _dex_t ncols) const
    {
        Matrix<Type> result(nrows, ncols);
        _dex_t count = 0; //counts through the dest array
        for (_dex_t i = row; i<row + nrows; ++i)
        {
            for (_dex_t j = col; j<col + ncols; ++j)
            {
                result[count] = (*this)(i, j);
                ++count;
            }
        }
        return result;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::inv() const
    {
        Matrix result = this->copy();
        //result.print();
        lapack_int *  pivotArray = (lapack_int *)mkl_malloc(_cols * sizeof(lapack_int), 64);
        matfacto(LAPACK_ROW_MAJOR, _rows, _cols, result.begin(), _cols, pivotArray);
        matinv(LAPACK_ROW_MAJOR, _rows, result.begin(), _cols, pivotArray);
        mkl_free(pivotArray);
        return result;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::exp() const
    {
        // Creating return container
        Matrix result(numRows(), numCols());
        // Size remains the same.
        // precision of the developpement.
        _dex_t accuracy = EXPONENTIAL_PRE;
        _dex_t N = _rows;
        _dex_t kind = sizeof(Type);

        //M_small = M/(2^N);
        complex* M_small = (complex*)mkl_malloc(length() * kind, 64);
        for (int i = 0; i < length(); i++) M_small[i] = (*_data)[i] / (real)pow(2.0, (real)N);

        // Exp part
        complex* m_exp1 = (complex*)mkl_malloc(length() * kind, 64);
        complex* m_exp2 = (complex*)mkl_malloc(length() * kind, 64);
        result.eye();

        complex* M_power = (complex*)mkl_malloc(length() * kind, 64);
        complex* M_power1 = (complex*)mkl_malloc(length() * kind, 64);
        // copymat(length, a, b) :: b = a
        copymat(_data->length(), M_small, 1, M_power, 1);

        complex* tmpM1 = (complex*)mkl_malloc(length() * kind, 64);

        real factorial_i = 1.0;

        for (int i = 1; i < accuracy; i++)
        {
            factorial_i *= i;

            //m_exp = m_exp + M_power/factorial[i];
            for (int x = 0; x < length(); x++) tmpM1[x] = M_power[x] / factorial_i;

            vecadd(length(), result.begin(), tmpM1, result.begin());


            //M_power = M_power * M_small;
            // copymat(length, a, b) :: b = a
            copymat(length(), M_power, 1, M_power1, 1);
            matmul(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                N, N, N, &BLAS_ALPHA, M_power1, N, M_small, N, &BLAS_BETA, M_power, N);
        }


        // Squaring step
        const MKL_INT oneb = 1;
        for (int i = 0; i < N; i++) {
            // m_exp = m_exp*m_exp;
            // copymat(length, a, b) :: b = a
            copymat(_data->length(), result.begin(), oneb, m_exp1, 1);
            // copymat(length, a, b) :: b = a
            copymat(_data->length(), result.begin(), 1, m_exp2, 1);
            matmul(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                N, N, N, &BLAS_ALPHA, m_exp1, N, m_exp2, N, &BLAS_BETA, result.begin(), N);
        }

        mkl_free(M_small);
        mkl_free(m_exp1);
        mkl_free(M_power);
        mkl_free(M_power1);
        mkl_free(tmpM1);
        mkl_free(m_exp2);

        return result;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::operator*(const Matrix& mat) const
    {
        Matrix result(_rows, mat._cols);
        result.zeros();

        matmul(CblasRowMajor,					// We work in c++ whith row-major matrices
            CblasNoTrans, CblasNoTrans,			// We do not need to transpose.
            _rows,								// Number of rows of the matrix A and C
            mat._cols,							// The number of columns of B and C
            _cols,								// The number of columns of A and Rows of B
            &BLAS_ALPHA,								// alpha = 1.0
            this->begin(),							// Getting a pointer to this matrix (A)
            _cols,								// Leading dimension of A
            mat.begin(),							// Getting a pointer to matrix B
            mat._cols,							// Leading dimension of matrix B
            &BLAS_BETA,								// beta = 0.0
            result.begin(),						// Pointer to result storage C
            mat._cols							// Leading dimension of C
        );
        return  result;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator=(const Matrix & rhs)
    {
        // If the rhs is just this object.
        if (&rhs == this)
            return *this;
        // Check if this matrix is properly allocated.
        if (!this->hasContent()) {
            //std::cout << "operator=  called - first allocation" << std::endl;
            this->init(rhs.numRows(), rhs.numCols());
        }
        else if (_data->length() != rhs._data->length())
        {
            Log::Fatal("Tried to assign different Matrices with inconistent length.");
            init(rhs.numRows(), rhs.numCols());
        }

        copymat(_data->length(), rhs.begin(), 1, begin(), 1);
        _name = rhs._name;
        return *this;
    }

    template<typename Type>
    inline Matrix<Type> & Matrix<Type>::operator*=(const Matrix & rhs)
    {
        matmul(CblasRowMajor,					// We work in c++ whith row-major matrices
            CblasNoTrans, CblasNoTrans,			// We do not need to transpose.
            _rows,								// Number of rows of the matrix A and C
            rhs._cols,							// The number of columns of B and C
            _cols,								// The number of columns of A and Rows of B
            &BLAS_ALPHA,								// alpha = 1.0
            this->begin(),								// Getting a pointer to this matrix (A)
            _cols,								// Leading dimension of A
            rhs.begin(),							// Getting a pointer to matrix B
            rhs._cols,							// Leading dimension of matrix B
            &BLAS_BETA,								// beta = 0.0
            this->begin(),								// Pointer to result storage C
            rhs._cols							// Leading dimension of C
        );
        return *this;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::operator*(const Type c) const
    {
        Matrix<Type> result(numRows(), numCols());
        copymat(_data->length(), _data->begin(), 1, result.begin(), 1);
        scalmat(_data->length(), &c, result.begin(), 1);
        return result;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::operator-(const Matrix& mat) const
    {
        Matrix<Type> result(numRows(), numCols());
        for (int i = 0; i < _data->length(); i++) {
            result[i] = (*this)[i] - mat[i];
        }
        return result;
    }

    template<typename Type>
    Matrix<Type> Matrix<Type>::operator+(const Matrix& mat) const
    {
        Matrix<Type> result(numRows(), numCols());
        for (int i = 0; i < _data->length(); i++) {
            result[i] = (*this)[i] + mat[i];
        }
        return result;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator+=(const Matrix& rhs)
    {
        vecadd(this->length(), this->begin(), rhs.begin(), this->begin());
        return (*this);
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator-=(const Matrix& rhs)
    {
        vecsub(this->length(), this->begin(), rhs.begin(), this->begin());
        return (*this);
    }


    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator = (const Type s)
    {
        ones();
        scale(s);
        return *this;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator += (const Type s)
    {
        for(int i = 0; i < _data->length(); ++i)
            _data[i] += s;
        return *this;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator -= (const Type s)
    {
        for(int i = 0; i < _data->length(); ++i)
            (*_data)[i] -= s;
        return *this;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator *= (const Type s)
    {
        scalmat(_data->length(), &s, _data->begin(), 1); 
        return *this;
    }

    template<typename Type>
    Matrix<Type>& Matrix<Type>::operator /= (const Type s)
    {
        complex div = 1.0/s;
        scalmat(_data->length(), &div, _data->begin(), 1); 
        return *this;
    }

    template<typename Type>
    inline void Matrix<Type>::eye()
    {
        std::fill(_data->begin(), _data->begin() + _data->length(), 0.0);
        for (int i = 0; i < _data->length(); i += _rows + 1)
        {
            (*_data)[i] = 1.0;
        }
    }

    template<typename Type>
    inline void Matrix<Type>::zeros() 
    { 
        std::fill(_data->begin(), _data->begin() + _data->length(), 0.0); 
    }

    template<typename Type>
    inline void Matrix<Type>::ones() 
    { 
        std::fill(_data->begin(), _data->begin() + _data->length(), 1.0); 
    }

    template<typename Type>
    inline void Matrix<Type>::scale(Type c) 
    { 
        scalmat(_data->length(), &c, _data->begin(), 1); 
    }

    template<typename Type>
    inline void Matrix<Type>::fill(real c) 
    { 
        ones(); 
        scale(c); 
    }
}