/**
 * @file
 * @brief Describes MatrixContainer Class
 * @author Nicolas Roy
 */
#pragma once

#include "mkl_config.h"
#include "Log.hpp"

template <typename Type>
class MatrixContainer
{
    public:
    /**
     * @brief There can not be a default Constructor
     * @details Or everything we are trying to put up here will go sideways
     */
        MatrixContainer() = delete;
    /**
     * @brief Init Constructor
     * @param length Data length
     */
        MatrixContainer(_dex_t length)
        {
             _length = length;
            this->allocate();
        }
    /**
     * @brief Main destructor
     * @details The container end-of-life will always come from a shared pointer.
     */
        ~MatrixContainer()
        {
            this->free();
        }

    /**
     * @brief Free MKL subsystem buffers
     */
        static void freeBuffers()
        {
            Log::Warning("[Info] Mkl internal buffers cleared.");
            mkl_free_buffers();
        }

        _dex_t length()
        {
            return _length;
        }
    /**
     * @brief Returns pointer to the first element of the memory segment.
     * @details This method is safe, if pointer is nullptr, program will terminate.
     * There is no way this method is gently called on an object without allocation.
     */
        Type* begin()
        {
            if(_data == nullptr)
                Log::Fatal("Headless memory.");
            return &(_data[0]);
        }
    /**
     * @brief Returns pointer to the first element of the memory segment.
     * @details This method is safe, if pointer is nullptr, program will terminate.
     * There is no way this method is gently called on an object without allocation.
     */
        Type* begin() const
        {
            if(_data == nullptr)
                Log::Fatal("Headless memory.");
            return &(_data[0]);
        }
    /**
     * @brief Performs deep copy of the data.
     */
        void copy(const MatrixContainer& source)
        {
            // copymat(length, a, b) :: b = a
            copymat(this->length(), source._data, 1, _data, 1);
        }

    public:
    /**
     * @brief Raw data Access
     * @param index unsigned index
     * @return constant reference to element
     */
        Type& operator[](const _dex_t index)
        {
            return this->_data[index];
        }
    /**
     * @brief Const Raw data Access
     * @param index unsigned index
     * @return constant reference to element
     */
        const Type& operator[](const _dex_t index) const
        {
            return this->_data[index];
        }

        

    private:
    /**
     * @brief Aligned memory allocator
     */
        void allocate()
        {
            _data = (Type*)mkl_malloc(_length * sizeof(Type), 64);
            if(_data == nullptr)
                Log::Fatal("Not able to allocate Matrix");
            _allocated = true;
        }
    /**
     * @brief Free Memory
     */
        void free()
        {
            if (_data != nullptr)
            {
                _allocated = false;
                mkl_free(_data);
                _data = nullptr;
            }
        }
        Type* _data;            ///< Aligned pointer to the data
        _dex_t _length;       ///< Data size
        bool _allocated;
};