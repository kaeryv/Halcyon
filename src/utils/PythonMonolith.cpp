#include "PythonMonolith.hpp"


PythonMonolith* PythonMonolith::m_Instance = nullptr;
std::map<std::string, PyObject *> PythonMonolith::m_LoadedModules = std::map<std::string, PyObject *>();
std::vector<PyObject*> PythonMonolith::m_ArgumentsBuffer = std::vector<PyObject*>();

PythonMonolith* PythonMonolith::Singleton()
{
    if (m_Instance ==  nullptr)
      m_Instance = new PythonMonolith();

    return m_Instance;
}

bool PythonMonolith::import(std::string module)
{
    PyObject *pyName, *pyModule, *pyDict;

    /** We require PyObect* string */
    pyName = PyString_FromString(module.c_str());

    /** Import module */
    pyModule = PyImport_Import(pyName);
    if(!pyModule)
        Log::Fatal("Failed to load module " + module);

    /** Generate Dic from module */
    pyDict = PyModule_GetDict(pyModule);

    m_LoadedModules[module] = pyDict;
}


std::string PythonMonolith::run(std::string module, std::string function)
{
    PyObject *pyFunc, *pyArgs, *pyArray;
    PyObject *pyDict = m_LoadedModules[module];

    pyFunc = PyDict_GetItemString(pyDict, function.c_str());
    if(!pyFunc){
        PyErr_Print();
        Log::Fatal("Could not load function " + function);
    }
    pyArgs= PyTuple_New(m_ArgumentsBuffer.size());

    for(std::size_t i = 0; i < m_ArgumentsBuffer.size(); ++i)
    {
        PyTuple_SetItem(pyArgs, i, m_ArgumentsBuffer[i]);
    }

    PyObject* pyResult = PyObject_CallObject(pyFunc, pyArgs);
    if(!pyResult){
        PyErr_Print();
        Log::Fatal(" ^ Python evaluation failed ^ ");
    }
    
    m_ArgumentsBuffer.clear();
    
    std::string message = PyString_AsString(pyResult);
    return message;
}

void PythonMonolith::bindDoubleVector(std::vector<double>& vector)
{
    PyObject *pyArray;
    pyArray = PyList_New(vector.size());
    
    for(std::size_t i = 0; i < vector.size(); ++i)
    {
        PyObject* temp = PyFloat_FromDouble(vector[i]);
        PyList_SetItem(pyArray, i, temp);
    }
    m_ArgumentsBuffer.push_back(pyArray);
}

void PythonMonolith::bindString(std::string string)
{
    PyObject *pyString;
    
    pyString = PyString_FromString(string.c_str());
    
    m_ArgumentsBuffer.push_back(pyString);
}

PythonMonolith::PythonMonolith()
{
    Py_Initialize();
    PyRun_SimpleString("import os");
    PyRun_SimpleString("print('[PYTHON] Monolith is instanciated.')");
}

PythonMonolith::~PythonMonolith()
{
    Py_Finalize();
}