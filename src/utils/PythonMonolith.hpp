#pragma once

#include <python2.7/Python.h>
#include "Log.hpp"

#include <string>
#include <memory>
#include <map>
#include <vector>

class PythonMonolith
{
    public:
        static PythonMonolith* Singleton();
        bool openLogFile(std::string logFile);
        void writeToLogFile();
        bool closeLogFile();
        std::string run(std::string module, std::string function);
        bool import(std::string module);

        void bindDoubleVector(std::vector<double>& vector);
        void bindString(std::string string);

    protected:
        PythonMonolith();
        ~PythonMonolith();
        PythonMonolith(PythonMonolith const&) {};  
        PythonMonolith& operator=(PythonMonolith const&) {};  
        static PythonMonolith* m_Instance;

        static std::vector<PyObject*> m_ArgumentsBuffer;

        static std::map<std::string, PyObject *> m_LoadedModules;
};