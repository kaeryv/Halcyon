#pragma once

#include "Matrix.hpp"
#include "GGrid.hpp"
#include "InputStorageContainer.hpp"

namespace Halcyon
{
    template<typename Type>
    class ThinMatrix : public Matrix<Type>
    {
        public:
                        ThinMatrix();
                        ThinMatrix(_dex_t length);

                        void init(const _dex_t length);
            virtual 	~ThinMatrix();


//            void 		build(const GGrid& g_vectors, JonesVector polarization);

            Type& operator () (const _dex_t index); 
            const Type& operator () (const _dex_t index) const;

            ThinMatrix& operator = (const Matrix<Type>& rhs)
            {
                // If the rhs is just this object.
                if (&rhs == this)
                    return *this;
                // Check if this matrix is properly allocated.
                if (!this->hasContent()) {
                    //std::cout << "operator=  called - first allocation" << std::endl;
                    Matrix<Type>::init(rhs.numRows(), rhs.numCols());
                }
                else if (this->length() != rhs.length())
                {
                    Log::Fatal("Tried to assign different Matrices with inconistent length.");
                    Matrix<Type>::init(rhs.numRows(), rhs.numCols());
                }

                if(this->numCols() != 1 && rhs.numRows() != 1)
                {
                    Log::Fatal("Right hand sizeis no vector type.");
                }

                this->_data->copy(*rhs._data);

                return *this;
            }
            
    };
}

#include "ThinMatrix.tpp"