#include "ThinMatrix.hpp"

namespace Halcyon
{
    template<typename Type>
    ThinMatrix<Type>::ThinMatrix() : Matrix<Type>()
    {

    }

    template<typename Type>
    ThinMatrix<Type>::ThinMatrix(_dex_t length) : Matrix<Type>(length, 1)
    {}

    template<typename Type>
    void ThinMatrix<Type>::init(const _dex_t length)
    {
        Matrix<Type>::init(length, 1);
    }
/*
    template<typename Type>
    void ThinMatrix<Type>::build(const GGrid & g_vectors, JonesVector polar)
    {
        Vec2<_dex_t> boundaries = g_vectors.getBoundaries();
        Vec2<_dex_t> dimensions = g_vectors.getDimensions();

        int ng = g_vectors.numVectors();
        int g0 = boundaries.x + dimensions.y * boundaries.y;

        this->init(4 * ng, 1);
        this->zeros();

        (*this)[g0]= polar.first; // s-polar
        (*this)[ng + g0] = polar.second; // p-polar
    }*/

    template<typename Type>
    ThinMatrix<Type>::~ThinMatrix()
    {

    }

    template<typename Type>
    Type& ThinMatrix<Type>::operator () (const _dex_t index)
    {
        return (*this)[index];
    }

    template<typename Type>
    const Type& ThinMatrix<Type>::operator () (const _dex_t index) const
    {
        return (*this)[index];
    }
}