#include "json.hpp"

namespace json
{
    bool IsStringValue(const json ref)
    {
        if(ref.is_string())
            return true;
    }

    bool IsKeyStringPresent(const json ref, std::string key)
    {
        if(ref[key].is_string())
            return true;
        return false;
    }


    bool IsKeyPresent(const json& ref, const std::string key)
    {
        // The json key contains field or subkey
        if(ref.count(json(key)) > 0 || ref[key].is_object())
            return true;
    
        return false;
    }

    bool IsKeyStringValue(const json& ref, const std::string key, const std::string value)
    {
        if(IsKeyPresent(ref, key) && IsStringValue(ref[key]) && !ref[key].get<std::string>().compare(value))
        {
            return true;
        }
        return false;
    }

    std::string KeyAsString(const json& ref, const std::string key)
    {
        return ref[key].get<std::string>();
    }
}