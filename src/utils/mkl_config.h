/**
 * \file
 * \brief Configures mkl environment.
 * \author Nicolas Roy
 * Never include mkl.h by other means than this file.
 * Precision of mkl functions and standard mkl interfaces are defined here.
 */

#pragma once




#include <complex>

/**
 * @brief Number of iterations of the Matrix Exponential.
 * @pre 3 is sufficient but you can push up to 10 easily.
 */
#define EXPONENTIAL_PRE 3


/**
 * @brief No reason to use float (will cause promotion spam)
 * But if you need it ...
 */
#define __KIND_DOUBLE__

/**
 * Because we are doing scientific stuff, always nice to work with clear types like real.
 */

#ifdef __KIND_DOUBLE__
    typedef double real;
    typedef std::complex<real> complex;
#else
    typedef float real;
    typedef std::complex<real> complex;
#endif

/**
 * @brief Index integer type.
 */
//typedef short _dex_t;            // Mean perf on test case 34.34 s
//typedef std::size_t _dex_t;    // Mean perf on test case 34.74 s
typedef int_fast32_t _dex_t;   // Mean perf on test case 34.84 s
//typedef int _dex_t;            // Mean perf on test case 37.74 s


/**
 * Defining mkl functions to create a standard AP interface
 */

#ifdef __KIND_DOUBLE__
#   define matmul			cblas_zgemm
#   define matfacto		    LAPACKE_zgetrf
#   define matinv			LAPACKE_zgetri
#   define vecadd			vzAdd
#   define vecsub           vzSub
#   define copymat			cblas_zcopy
#   define scalmat			cblas_zscal
#   define allocate_(x,y)	mkl_malloc(x,y,64)
#else
#   define matmul           cblas_cgemm
#   define matfacto         LAPACKE_cgetrf
#   define matinv           LAPACKE_cgetri
#   define vecadd           vcAdd
#   define copymat          cblas_ccopy
#   define scalmat	        cblas_csscal
#   define allocate_(x,y)	mkl_malloc(x,y,64)
#endif

/**
 * Defining mkl complex type
 */

#ifdef __KIND_DOUBLE__
#   define MKL_Complex16 std::complex<double>
#else
#   define MKL_Complex8 std::complex<float>
#endif


/**
 * Only including mkl.h after those presets
 */
//#define MKL_DIRECT_CALL
//#include <mkl_direct_call.h>
#include <mkl.h>